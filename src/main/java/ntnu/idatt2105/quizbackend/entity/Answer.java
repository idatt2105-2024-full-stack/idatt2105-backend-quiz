package ntnu.idatt2105.quizbackend.entity;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Entity class for Answer
 * An answer has a unique id, a text and a boolean to check if it is correct
 * An answer is connected to a question and an attempt
 *
 * @Entity Indicates that this class is an entity class
 * @Table Specifies the primary table for the annotated entity
 * @Getter Lombok will generate the getters for all attributes in the class
 * @Setter Lombok will generate the setters for all attributes in the class
 */
@Entity(name = "Answer")
@Table(name = "answer")
@Getter
@Setter
@NoArgsConstructor
public class Answer {

  /**
   * Variable to store the answer id, which is a unique identifier for the answer.
   * Generated value strategy is set to identity, which means that the value is automatically generated.
   * The id is the primary key for the table. The id is a Long.
   *
   * @Id Indicates that the attribute is a primary key
   * @GeneratedValue Indicates the primary key generation strategy
   * @Column Indicates that the attribute is a column in the table
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "answer_id")
  private Long id;

  /**
   * Variable used to check if the answer is correct.
   * The correctAnswer is a boolean.
   */
  private boolean correctAnswer;

  /**
   * Variable to store the text of the answer.
   * The text is a string.
   */
  private String text;

  /**
   * Constructor for Answer. The constructor takes a text and a boolean to check if the answer is correct.
   * @param answerText The text of the answer
   * @param isCorrect A boolean to check if the answer is correct
   */
  public Answer(String answerText, boolean isCorrect) {
    text = answerText;
    correctAnswer = isCorrect;
  }

  /**
   * Variable to store the question the answer belongs to.
   * The question is a Question object.
   *
   * @ManyToOne Indicates that many answers can belong to one question
   * @JoinColumn Indicates that the question_id is a foreign key in the answer table
   * @see ntnu.idatt2105.quizbackend.entity.Question
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "question_id", nullable = false)
  private Question question;
}
