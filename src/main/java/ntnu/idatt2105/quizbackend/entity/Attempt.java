package ntnu.idatt2105.quizbackend.entity;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Entity class for Attempt
 * Represents an attempt to answer a quiz
 * Contains the score, date, attempter and chosen answers
 *
 * @Entity Specifies that the class is an entity
 * @Table Specifies the name of the table in the database
 * @Getter Lombok will generate the getters for all attributes in the class
 * @Setter Lombok will generate the setters for all attributes in the class
 */
@Entity(name = "Attempt")
@Table(name = "attempt")
@Getter
@Setter
public class Attempt {

    /**
     * Variable to store the attempt id, which is a unique identifier for the attempt.
     * Generated value strategy is set to identity, which means that the value is automatically generated.
     * The id is the primary key for the table. The id is a Long.
     *
     * @Id Indicates that the attribute is a primary key
     * @GeneratedValue Indicates the primary key generation strategy
     * @Column Indicates that the attribute is a column in the table
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "attempt_id")
    private Long id;

    /**
     * Variable to store the score of the attempt.
     * The score is an integer.
     */
    private int score;

    /**
     * Variable to store the name of the quiz the attempt belongs to.
     * The quizName is a String.
     */
    private String quizName;


    /**
     * Variable to store the date of the attempt.
     * The date is a Date object.
     *
     * @Temporal Indicates that the attribute is a date
     */
    @Temporal(TemporalType.DATE)
    private Date date;

    /**
     * Variable to store the user that attempted the quiz.
     * The attempter is a User object.
     *
     * @ManyToOne Indicates that many attempts can belong to one user
     * @JoinColumn Indicates that the attempter is a foreign key in the attempt table
     * @see ntnu.idatt2105.quizbackend.entity.User
     */
    @ManyToOne
    @JoinColumn(name="attempter", nullable=false)
    private User attempter;

    /**
     * Variable to store the quiz the attempt belongs to.
     * The quiz is a Quiz object.
     *
     * @ManyToOne Indicates that many attempts can belong to one quiz
     * @JoinColumn Indicates that the quiz_id is a foreign key in the attempt table
     * @see ntnu.idatt2105.quizbackend.entity.Quiz
     */
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "attempt_answers",
            joinColumns = @JoinColumn(name = "attempt_id"),
            inverseJoinColumns = @JoinColumn(name = "answer_id"))
    private List<Answer> chosen_answers = new ArrayList<>();

    /**
     * Variable to store the quiz the attempt belongs to.
     * The quiz is a Quiz object.
     *
     * @ManyToOne Indicates that many attempts can belong to one quiz
     * @JoinColumn Indicates that the quiz_id is a foreign key in the attempt table
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "quiz_id")
    private Quiz quiz;
}
