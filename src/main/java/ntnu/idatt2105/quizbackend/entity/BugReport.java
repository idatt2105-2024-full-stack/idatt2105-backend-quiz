package ntnu.idatt2105.quizbackend.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;


/**
 * Entity class for BugReport
 * Represents a bug report in the database
 * Contains the bug id, text and the reporter
 *
 * @Entity Lombok will generate the getters for all attributes in the class
 * @Table Lombok will generate the setters for all attributes in the class
 * @Getter Lombok will generate the getters for all attributes in the class
 * @Setter Lombok will generate the setters for all attributes in the class
 */
@Entity
@Table(name = "bug_report")
@Getter
@Setter
public class BugReport {

    /**
     * Variable to store the bug id, which is a unique identifier for the bug report.
     * Generated value strategy is set to identity, which means that the value is automatically generated.
     * The id is the primary key for the table. The id is a Long.
     *
     * @Id Indicates that the attribute is a primary key
     * @GeneratedValue Indicates the primary key generation strategy
     * @Column Indicates that the attribute is a column in the table
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "bug_id")
    private Long id;

    /**
     * Variable to store the text of the bug report.
     * The text is a string.
     */
    private String text;

    /**
     * Variable to store the user that reported the bug.
     * The reporter is a User object.
     *
     * @ManyToOne Indicates that many bug reports can belong to one user
     * @JoinColumn Indicates that the reporter is a foreign key in the bug report table
     * @see ntnu.idatt2105.quizbackend.entity.User
     */
    @ManyToOne
    @JoinColumn(name="reporter")
    private User reporter;
}
