package ntnu.idatt2105.quizbackend.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Entity class for Quiz
 * Represents a quiz
 * Contains the name, description, difficulty, thumbnail, creator, questions and tags
 *
 * @Entity Specifies that the class is an entity
 * @Table Specifies the name of the table in the database
 * @Getter Lombok will generate the getters for all attributes in the class
 * @Setter Lombok will generate the setters for all attributes in the class
 */
@Entity(name = "Quiz")
@Table(name = "quiz")
@Getter
@Setter
public class Quiz {

  /**
   * Variable to store the quiz id, which is a unique identifier for the quiz.
   * Generated value strategy is set to identity, which means that the value is automatically generated.
   * The id is the primary key for the table. The id is a Long.
   *
   * @Id Indicates that the attribute is a primary key
   * @GeneratedValue Indicates the primary key generation strategy
   * @Column Indicates that the attribute is a column in the table
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "quiz_id")
  private Long id;

  /**
   * Variable to store the name of the quiz.
   * The name is a string.
   */
  private String name;

  /**
   * Variable to store the description of the quiz.
   * The description is a string.
   */
  private String description;

  /**
   * Variable to store the difficulty of the quiz.
   * The difficulty is an integer.
   * The difficulty is a number between 1 and 5.
   */
  private int difficulty;

  /**
   * Variable to store the thumbnail of the quiz.
   * The thumbnail is a string.
   */
  private String thumbnail;

  /**
   * Variable to store the creator of the quiz.
   * The creator is a User object.
   *
   * @ManyToOne Indicates that many quizzes can belong to one user
   * @JoinColumn Indicates that the user_id is a foreign key in the quiz table
   * @see ntnu.idatt2105.quizbackend.entity.User
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id")
  private User creator;

  /**
   * Variable to store the questions of the quiz.
   * The questions is a set of Question objects.
   * A quiz can have many questions.
   *
   * @ManyToMany Indicates that many quizzes can belong to many questions
   * @JoinTable Specifies the join table for the relationship
   * @JoinColumn Indicates that the quiz_id is a foreign key in the quiz_questions table
   * @JoinColumn Indicates that the question_id is a foreign key in the quiz_questions table
   * @see ntnu.idatt2105.quizbackend.entity.Question
   */
  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(
          name = "quiz_questions",
          joinColumns = @JoinColumn(name = "quiz_id"),
          inverseJoinColumns = @JoinColumn(name = "question_id"))
  private List<Question> questions;

  /**
   * Variable to store the tags of the quiz.
   * The tags is a set of Tag objects.
   * A quiz can have many tags.
   *
   * @ManyToMany Indicates that many quizzes can have many tags
   * @JoinTable Specifies the join table for the relationship
   * @JoinColumn Indicates that the quiz_id is a foreign key in the quiz_tags table
   * @JoinColumn Indicates that the tag_id is a foreign key in the quiz_tags table
   * @see ntnu.idatt2105.quizbackend.entity.Tag
   */
  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(
          name = "quiz_tags",
          joinColumns = @JoinColumn(name = "quiz_id"),
          inverseJoinColumns = @JoinColumn(name = "tag_id"))
  private List<Tag> tags;

  /**
   * Variable to store the attempts of the quiz.
   * The attempts is a list of Attempt objects.
   * A quiz can have many attempts.
   *
   * @OneToMany Indicates that one quiz can have many attempts
   * @see ntnu.idatt2105.quizbackend.entity.Attempt
   */
  @OneToMany(mappedBy = "quiz", fetch = FetchType.LAZY)
  private List<Attempt> attempts;

  /**
   * A method to convert the object to a string
   *
   * @return A string representation of the object
   */
  @Override
  public String toString() {
    return "{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", description='" + description + '\'' +
            ", difficulty=" + difficulty +
            ", thumbnail='" + thumbnail + '\'' +
            ", creator=" + creator +
            ", questions=" + questions +
            ", tags=" + tags +
            '}';
  }
}
