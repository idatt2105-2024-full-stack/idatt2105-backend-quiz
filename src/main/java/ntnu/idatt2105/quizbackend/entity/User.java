package ntnu.idatt2105.quizbackend.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Set;

/**
 * Entity class for User
 * Represents a user
 * Contains the email, username, password, token, quizzes, attempts, bug reports and questions
 *
 * @Entity Specifies that the class is an entity
 * @Table Specifies the name of the table in the database
 * @Getter Lombok will generate the getters for all attributes in the class
 * @Setter Lombok will generate the setters for all attributes in the class
 */
@Entity
@Table(name = "users")
@Getter
@Setter
public class User {

  /**
   * Variable to store the user id, which is a unique identifier for the user.
   * Generated value strategy is set to identity, which means that the value is automatically generated.
   * The id is the primary key for the table. The id is a Long.
   *
   * @Id Indicates that the attribute is a primary key
   * @GeneratedValue Indicates the primary key generation strategy
   * @Column Indicates that the attribute is a column in the table
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  /**
   * Variable to store the email of the user.
   * The email is a string.
   */
  private String email;

  /**
   * Variable to store the username of the user.
   * The username is a string.
   */
  private String username;

  /**
   * Variable to store the password of the user.
   * The password is a string.
   */
  private String password;

  /**
   * Variable to store the token of the user.
   * The token is a string.
   */
  private String token;

  /**
   * Variable to store the quizzes that the user has created.
   * The quizzes is a set of Quiz objects.
   * A user can create many quizzes.
   *
   * @OneToMany Indicates that one user can create many quizzes
   * @JoinColumn Indicates that the creator is a foreign key in the quiz table
   * @see ntnu.idatt2105.quizbackend.entity.Quiz
   */
  @OneToMany(mappedBy = "creator", orphanRemoval = false, fetch = FetchType.LAZY)
  private List<Quiz> quizzes;

  /**
   * Variable to store the questions that the user has created.
   * The questions is a list of Question objects.
   * A user can create many questions.
   *
   * @OneToMany Indicates that one user can create many questions
   * @JoinColumn Indicates that the creator is a foreign key in the question table
   * @see ntnu.idatt2105.quizbackend.entity.Question
   */
  @OneToMany(mappedBy = "creator", orphanRemoval = false, fetch = FetchType.LAZY)
  private List<Question> questions;

  /**
   * Variable to store the attempts that the user has made.
   * The attempts is a set of Attempt objects.
   * A user can make many attempts.
   *
   * @OneToMany Indicates that one user can make many attempts
   * @see ntnu.idatt2105.quizbackend.entity.Attempt
   */
  @OneToMany(mappedBy = "attempter", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)
  private List<Attempt> attempts;

  /**
   * Variable to store the bug reports that the user has made.
   * The bugReports is a list of BugReport objects.
   * A user can make many bug reports.
   *
   * @OneToMany Indicates that one user can make many bug reports
   * @see ntnu.idatt2105.quizbackend.entity.BugReport
   */
  @OneToMany(mappedBy = "reporter", fetch = FetchType.LAZY)
  private List<BugReport> bugReports;
}
