package ntnu.idatt2105.quizbackend.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Entity class for Tag
 * Represents a tag
 * Contains the tag id, the tag, questions and quizzes
 *
 * @Entity Specifies that the class is an entity
 * @Table Specifies the name of the table in the database
 * @Getter Lombok will generate the getters for all attributes in the class
 * @Setter Lombok will generate the setters for all attributes in the class
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class Tag {

    /**
     * Variable to store the tag id, which is a unique identifier for the tag.
     * Generated value strategy is set to identity, which means that the value is automatically generated.
     * The id is the primary key for the table. The id is a Long.
     *
     * @Id Indicates that the attribute is a primary key
     * @GeneratedValue Indicates the primary key generation strategy
     * @Column Indicates that the attribute is a column in the table
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "tag_id")
    private Long id;

    /**
     * Variable to store the tag text.
     * The tag is a string.
     */
    private String tag;

    /**
     * Constructor for Tag
     * @param tagText The text of the tag
     */
    public Tag(String tagText) {
        this.tag = tagText;
    }

    /**
     * Variable to store the questions that the tag belongs to.
     * The questions is a list of Question objects.
     * A tag can belong to many questions.
     *
     * @ManyToMany Indicates that many tags can belong to many questions
     * @see ntnu.idatt2105.quizbackend.entity.Question
     */
    @ManyToMany(mappedBy = "tags", fetch = FetchType.LAZY)
    private List<Question> questions;

    /**
     * Variable to store the quizzes that the tag belongs to.
     * The quizzes is a list of Quiz objects.
     * A tag can belong to many quizzes.
     *
     * @ManyToMany Indicates that many tags can belong to many quizzes
     * @see ntnu.idatt2105.quizbackend.entity.Quiz
     */
    @ManyToMany(mappedBy = "tags", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Quiz> quizzes;
}
