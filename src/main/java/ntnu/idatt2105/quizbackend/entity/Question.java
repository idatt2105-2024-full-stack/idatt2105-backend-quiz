package ntnu.idatt2105.quizbackend.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Entity class for Question
 * Represents a question in a quiz
 * Contains the text, resource url, quizzes, answers, tags and creator
 *
 * @Entity Specifies that the class is an entity
 * @Table Specifies the name of the table in the database
 * @Getter Lombok will generate the getters for all attributes in the class
 * @Setter Lombok will generate the setters for all attributes in the class
 */
@Entity(name = "Question")
@Table(name = "question")
@Getter
@Setter
public class Question {

  /**
   * Variable to store the question id, which is a unique identifier for the question.
   * Generated value strategy is set to identity, which means that the value is automatically generated.
   * The id is the primary key for the table. The id is a Long.
   *
   * @Id Indicates that the attribute is a primary key
   * @GeneratedValue Indicates the primary key generation strategy
   * @Column Indicates that the attribute is a column in the table
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "question_id")
  private Long id;

  /**
   * Variable to store the text of the question.
   * The text is a string.
   */
  private String text;

  /**
   * Variable to store the resource url of the question.
   * The resource url is a string.
   */
  private String resource_url;

  /**
   * Variable to store the quizzes that the question belongs to.
   * The quizzes is a set of Quiz objects.
   * A question can belong to many quizzes.
   *
   * @ManyToMany Indicates that many questions can belong to many quizzes
   * @see ntnu.idatt2105.quizbackend.entity.Quiz
   */
  @ManyToMany(mappedBy = "questions", fetch = FetchType.LAZY)
  private List<Quiz> quizzes;

  /**
   * Variable to store the answers of the question.
   * The answers is a list of Answer objects.
   * A question can have many answers.
   *
   * @OneToMany Indicates that one question can have many answers
   * @see ntnu.idatt2105.quizbackend.entity.Answer
   */
  @OneToMany(mappedBy = "question", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  private List<Answer> answers;

  /**
   * Variable to store the tags of the question.
   * The tags is a set of Tag objects.
   * A question can have many tags.
   *
   * @ManyToMany Indicates that many questions can have many tags
   * @JoinTable Specifies the join table for the relationship
   * @JoinColumn Indicates that the question_id is a foreign key in the question_tags table
   * @JoinColumn Indicates that the tag_id is a foreign key in the question_tags table
   * @see ntnu.idatt2105.quizbackend.entity.Tag
   */
  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(
          name = "question_tags",
          joinColumns = @JoinColumn(name = "question_id"),
          inverseJoinColumns = @JoinColumn(name = "tag_id"))
  private List<Tag> tags;

  /**
   * Variable to store the creator of the question.
   * The creator is a User object.
   *
   * @ManyToOne Indicates that many questions can belong to one user
   * @JoinColumn Indicates that the user_id is a foreign key in the question table
   * @see ntnu.idatt2105.quizbackend.entity.User
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id")
  private User creator;

  /**
   * Method to convert the object to a string
   * Used for debugging
   *
   * @return A string representation of the object
   */
  @Override
  public String toString() {
    return "{" +
            "id=" + id +
            ", text='" + text + '\'' +
            ", resource_url='" + resource_url + '\'' +
            ", quizzes=" + quizzes +
            ", answers=" + answers +
            ", tags=" + tags +
            ", creator=" + creator +
            '}';
  }
}
