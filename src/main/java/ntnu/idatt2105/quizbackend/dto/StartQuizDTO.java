package ntnu.idatt2105.quizbackend.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Data transfer object for starting a quiz
 *
 * @Getter Lombok will generate the getters for all attributes in the class
 * @Setter Lombok will generate the setters for all attributes in the class
 * @AllArgsConstructor Lombok will generate a constructor with all attributes in the class as parameters
 * @NoArgsConstructor Lombok will generate an empty constructor
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StartQuizDTO {

  /**
   * The id of the quiz to start
   */
  int quizId;
}
