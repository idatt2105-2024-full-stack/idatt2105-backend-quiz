package ntnu.idatt2105.quizbackend.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ntnu.idatt2105.quizbackend.entity.User;

/**
 * Data transfer object for creating a new user
 *
 * @AllArgsConstructor Lombok will generate a constructor for all fields in the class
 * @NoArgsConstructor Lombok will generate an empty constructor
 * @Getter Lombok will generate getters for all fields in the class
 * @Setter Lombok will generate setters for all fields in the class
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class NewUserDTO {

  /**
   * The email of the user
   */
  private String email;

  /**
   * The username of the user
   */
  private String username;

  /**
   * The password of the user
   */
  private String password;

  /**
   * Converts the NewUserDTO to a User object
   *
   * @return a User object
   */
  public User toNewUserEntry() {
    User newUser = new User();

    newUser.setEmail(getEmail());
    newUser.setUsername(getUsername());
    newUser.setPassword(getPassword());

    return newUser;
  }
}
