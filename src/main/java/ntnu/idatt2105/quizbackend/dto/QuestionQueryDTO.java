package ntnu.idatt2105.quizbackend.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

/**
 * Data transfer object for QuestionQuery
 * Used to transfer data between the frontend and the backend
 * Contains the first, count, id and tags
 *
 * @NoArgsConstructor Lombok will generate a constructor with no arguments
 * @AllArgsConstructor Lombok will generate a constructor with all arguments
 * @Getter Lombok will generate the getters for all attributes in the class
 * @Setter Lombok will generate the setters for all attributes in the class
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class QuestionQueryDTO {

    /**
     * The first question
     */
    long first;

    /**
     * The count of questions
     */
    long count;

    /**
     * The id of the question
     */
    long id;

    /**
     * Set of tag ids belonging to the question
     */
    Set<Long> tags;
}
