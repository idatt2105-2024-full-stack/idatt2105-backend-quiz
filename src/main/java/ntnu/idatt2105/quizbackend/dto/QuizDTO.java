package ntnu.idatt2105.quizbackend.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Data transfer object for Quiz
 * Used to transfer data between the frontend and the backend
 * Contains the name, description, difficulty, thumbnail URI, question ids and tag ids
 *
 * @Getter Lombok will generate the getters for all attributes in the class
 * @Setter Lombok will generate the setters for all attributes in the class
 */
@Getter
@Setter
public class QuizDTO {

    /**
     * Variable to store the id of the quiz
     */
    private Long id;

    /**
     * Variable to store the name of the quiz
     */
    private String name;

    /**
     * Variable to store the description of the quiz
     */
    private String description;

    /**
     * Variable to store the difficulty of the quiz
     * The difficulty is a number between 1 and 5
     */
    private int difficulty;

    /**
     * Variable to store the thumbnail URI, which is a link to a picture or video
     */
    private String thumbnail_URI;

    /**
     * Variable to store the creator id of the quiz
     */
    private Long creator_id;

    /**
     * List of question ids belonging to the quiz
     */
    private List<Long> question_ids;

    /**
     * List of tag ids belonging to the quiz
     */
    private List<Long> tag_ids;

    /**
     * Method to convert the object to a string
     *
     * @return A string representation of the object
     */
    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", difficulty=" + difficulty +
                ", thumbnail_URI='" + thumbnail_URI + '\'' +
                ", question_ids=" + question_ids +
                ", tag_ids=" + tag_ids +
                '}';
    }
}
