package ntnu.idatt2105.quizbackend.dto;

import lombok.*;

/**
 * Data transfer object for Answer
 * Used to transfer data between the frontend and the backend
 * Contains the answer and if it is correct
 *
 * @NoArgsConstructor The Lombok annotation will generate a constructor with no parameters
 * @AllArgsConstructor The Lombok annotation will generate a constructor with all parameters
 * @Getter The Lombok annotation will generate the getters for all attributes in the class
 * @Setter The Lombok annotation will generate the setters for all attributes in the class
 * @EqualsAndHashCode The Lombok annotation will generate the equals and hashCode methods for the class
 * @see ntnu.idatt2105.quizbackend.entity.Answer
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class AnswerDTO {

  /**
   * Variable to store the answer text
   */
  private String answer;

  /**
   * Variable to check if the answer is correct
   */
  private boolean isCorrect;
}
