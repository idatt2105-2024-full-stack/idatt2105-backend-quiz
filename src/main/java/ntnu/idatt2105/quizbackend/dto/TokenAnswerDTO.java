package ntnu.idatt2105.quizbackend.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Data transfer object for TokenAnswer
 * Used to transfer data between the frontend and the backend
 * Contains a boolean success and a token
 *
 * @Getter Lombok will generate the getters for all attributes in the class
 * @Setter Lombok will generate the setters for all attributes in the class
 * @AllArgsConstructor Lombok will generate a constructor with all arguments
 * @NoArgsConstructor Lombok will generate a constructor with no arguments
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TokenAnswerDTO {

    /**
     * Variable to store the success of the token.
     * If the token is valid, success is true, otherwise false
     */
    private boolean success;

    /**
     * Variable to store the token, which is a string that is used to authenticate the user
     */
    private String token;
}