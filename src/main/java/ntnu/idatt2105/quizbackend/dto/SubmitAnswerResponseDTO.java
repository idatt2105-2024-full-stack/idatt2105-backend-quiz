package ntnu.idatt2105.quizbackend.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Data transfer object for submitting an answer
 *
 * @Getter Lombok will generate the getters for all attributes in the class
 * @Setter Lombok will generate the setters for all attributes in the class
 * @AllArgsConstructor Lombok will generate a constructor with all attributes in the class
 * @NoArgsConstructor Lombok will generate an empty constructor
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SubmitAnswerResponseDTO {

    /**
     * The id of the question that the answer is submitted for
     */
    int score;

    /**
     * The gain of the question
     */
    int gain;

    /**
     * The next question
     */
    QuestionDTO next;
}
