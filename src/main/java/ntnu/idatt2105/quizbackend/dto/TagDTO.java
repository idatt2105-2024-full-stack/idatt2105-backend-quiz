package ntnu.idatt2105.quizbackend.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Data transfer object for Tag
 * Used to transfer data between the frontend and the backend
 * Contains the tag id and the tag
 *
 * @Getter Lombok will generate the getters for all attributes in the class
 * @Setter Lombok will generate the setters for all attributes in the class
 */
@Getter
@Setter
public class TagDTO {

  /**
   * Variable to store the tag id, which is a unique identifier for the tag
   */
  private Long id;

  /**
   * Variable to store the tag, which is a string that describes the tag
   */
  private String tag;
}
