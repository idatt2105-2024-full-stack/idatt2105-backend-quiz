package ntnu.idatt2105.quizbackend.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Data transfer object for User
 * Used to transfer data between the frontend and the backend
 * Contains the email, username, quizzes, questions and attempts
 *
 * @Getter Lombok will generate the getters for all attributes in the class
 * @Setter Lombok will generate the setters for all attributes in the class
 * @AllArgsConstructor Lombok will generate a constructor with all arguments
 * @NoArgsConstructor Lombok will generate a constructor with no arguments
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {

    /**
     * Variable to store the email of the user
     */
    private String email;

    /**
     * Variable to store the username of the user
     */
    private String username;

    /**
     * Variable to store the quizzes the user has created
     */
    private List<QuizDTO> quizzes;

    /**
     * Variable to store the questions the user has created
     */
    private List<QuestionDTO> questions;

    /**
     * Variable to store the attempts the user has made
     */
    private List<AttemptDTO> attempts;
}
