package ntnu.idatt2105.quizbackend.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * Data transfer object for Attempt
 * Used to transfer data between the frontend and the backend
 * Contains the quizId, score, quizName, date and attempter
 *
 * @NoArgsConstructor The Lombok annotation will generate a constructor with no parameters
 * @AllArgsConstructor The Lombok annotation will generate a constructor with all parameters
 * @Getter The Lombok annotation will generate the getters for all attributes in the class
 * @Setter The Lombok annotation will generate the setters for all attributes in the class
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AttemptDTO {

    /**
     * Variable to store the quizId
     */
    private long quizId;

    /**
     * Variable to store the score
     */
    private int score;

    /**
     * Variable to store the quizName
     */
    private String quizName;

    /**
     * Variable to store the date
     */
    private long date;

    /**
     * Variable to store the attempter
     */
    private long attempter;
}
