package ntnu.idatt2105.quizbackend.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Data transfer object for authenticating a user
 *
 * @AllArgsConstructor Lombok will generate a constructor which takes all class attributes as arguments
 * @Getter Lombok will generate getters for all attributes in the class
 * @Setter Lombok will generate setters for all attributes in the class
 */
@AllArgsConstructor
@Getter
@Setter
public class AuthenticateUserDTO {

    /**
     * The email of the user
     */
    private String email;

    /**
     * The password of the user
     */
    private String password;
}
