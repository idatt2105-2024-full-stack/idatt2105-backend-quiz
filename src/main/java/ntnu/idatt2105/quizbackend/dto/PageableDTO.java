package ntnu.idatt2105.quizbackend.dto;

import jakarta.annotation.Nullable;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Data transfer object for pageable requests
 *
 * @Getter Lombok will generate getters for all fields in the class
 * @Setter Lombok will generate setters for all fields in the class
 */
@Getter
@Setter
public class PageableDTO {

  /**
   * The page number
   */
  private int page;

  /**
   * The size of the page
   */
  private int size;

  /**
   * List of tag ids. Can be null or empty
   */
  @Nullable
  private List<Long> tagIds;
}
