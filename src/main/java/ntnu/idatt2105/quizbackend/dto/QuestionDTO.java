package ntnu.idatt2105.quizbackend.dto;

import lombok.*;

import java.util.List;

/**
 * Data transfer object for Question
 * Used to transfer data between the frontend and the backend
 * Contains the question, contentURI, possible answers and tag ids
 *
 * @NoArgsConstructor Lombok will generate a constructor with no arguments
 * @AllArgsConstructor Lombok will generate a constructor with all arguments
 * @Getter Lombok will generate the getters for all attributes in the class
 * @Setter Lombok will generate the setters for all attributes in the class
 * @EqualsAndHashCode Lombok will generate the equals and hashCode methods for the class
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class QuestionDTO {

  /**
   * The primary key of this Question in the database
   */
  private long id;

  /**
   * Variable to store the question text
   */
  private String question;

  /**
   * Variable to store the contentURI, which is a link to a picture or video
   */
  private String contentURI;

  /**
   * List of possible answers for the question
   */
  private List<AnswerDTO> possibleAnswers;

  /**
   * Set of tag ids belonging to the question
   */
  private List<Long> tag_ids;
}
