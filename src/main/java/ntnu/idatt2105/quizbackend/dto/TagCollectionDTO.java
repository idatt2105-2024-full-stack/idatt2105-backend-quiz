package ntnu.idatt2105.quizbackend.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.List;

/**
 * Data transfer object for a collection of tags.
 * This class is used to wrap a list of tags inside an object
 * making it easier to handle for both frontend and backend implementation.
 *
 * @Getter Lombok will generate the getters for all attributes in the class
 * @Setter Lombok will generate the setters for all attributes in the class
 * @NoArgsConstructor Lombok will generate an empty constructor
 * @AllArgsConstructor Lombok will generate a constructor with all attributes in the class
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TagCollectionDTO {

    /**
     * A list of tags
     */
    List<TagDTO> tags;
}
