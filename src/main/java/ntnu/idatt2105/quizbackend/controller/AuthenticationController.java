package ntnu.idatt2105.quizbackend.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import ntnu.idatt2105.quizbackend.dto.AuthenticateUserDTO;
import ntnu.idatt2105.quizbackend.dto.NewUserDTO;
import ntnu.idatt2105.quizbackend.dto.TokenAnswerDTO;
import ntnu.idatt2105.quizbackend.security.SecretsConfig;
import ntnu.idatt2105.quizbackend.service.AuthenticationService;
import ntnu.idatt2105.quizbackend.util.JwtUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

/**
 * The AuthenticationController class is a REST controller that handles all requests related to authentication.
 * It is responsible for registering new users, logging in users and creating anonymous users.
 * It also has a method for testing purposes.
 *
 * @RestController The Lombok annotation tells Spring that this class will serve REST API endpoints.
 * @CrossOrigin The Lombok annotation tells Spring to allow requests from any origin
 * @RequestMapping The Lombok annotation is used to map web requests to specific handler classes and/or handler methods.
 */

@RestController
@CrossOrigin
@RequestMapping("/api/auth")
public class AuthenticationController {

  /**
   * The SecretsConfig object is used to access the salt and JWT value.
   */
  private final SecretsConfig secrets;

  /**
   * The JwtUtil object is used to create JWT tokens.
   */
  private final JwtUtil jwtUtil;

  /**
   * The AuthenticationService object is used to access the methods in the AuthenticationService class.
   */
  private final AuthenticationService authService;

  private long anonymousIdCounter = 0;

  /**
   * Constructor for the AuthenticationController
   *
   * @param secrets is a SecretsConfig object
   * @param jwtUtil is a JwtUtil object
   * @param authService is an AuthenticationService object
   */
  public AuthenticationController(SecretsConfig secrets, JwtUtil jwtUtil, AuthenticationService authService) {
    this.secrets = secrets;
    this.jwtUtil = jwtUtil;
    this.authService = authService;
  }


  @Operation(summary = "Register a new user", description = "Register a new user with the given email and password")
  @ApiResponses(value = {
          @ApiResponse(
                  responseCode = "201",
                  description = "User created",
                  content = {
                          @Content(mediaType = "application/json",
                                  schema = @Schema(implementation = TokenAnswerDTO.class)),
                  }),
          @ApiResponse(
                  responseCode = "400",
                  description = "Invalid input or invalid email format",
                  content = @Content),
          @ApiResponse(
                  responseCode = "409",
                  description = "User already exists",
                  content = @Content),
  })
  @Parameter(
          name = "NewUserDTO",
          description = "NewUserDTO containing all the information related to a user",
          required = true,
          content = {
                  @Content(mediaType = "application/json",
                          schema = @Schema(implementation = NewUserDTO.class)
                  )})
  @PostMapping("/register")
  public ResponseEntity<TokenAnswerDTO> registerNewUser(@RequestBody NewUserDTO dto) {
    if (!authService.validateInputEmail(dto.getEmail()))
      return ResponseEntity.badRequest().body(new TokenAnswerDTO(false, null));

    if (authService.userExists(dto.getEmail()))
      return ResponseEntity.status(HttpStatus.CONFLICT).body(new TokenAnswerDTO(false, null));

    final Long userId = authService.registerNewUser(dto);
    return ResponseEntity.created(URI.create("/api/user/")).body(new TokenAnswerDTO(true, jwtUtil.createUserToken(userId)));
  }

  @Operation(summary = "Login", description = "Login with the given email and password")
  @ApiResponses(value = {
          @ApiResponse(
                  responseCode = "200",
                  description = "User logged in",
                  content = {
                          @Content(mediaType = "application/json",
                                  schema = @Schema(implementation = TokenAnswerDTO.class)),
                  }),
          @ApiResponse(
                  responseCode = "400",
                  description = "Invalid input or invalid email format",
                  content = @Content),
          @ApiResponse(
                  responseCode = "401",
                  description = "Unauthorized access returned as a result of invalid password",
                  content = @Content),
  })
  @Parameter(
          name = "AuthenticateUserDTO",
          description = "AuthenticateUserDTO containing all the information related to a user",
          required = true,
          content = {
                  @Content(mediaType = "application/json",
                          schema = @Schema(implementation = AuthenticateUserDTO.class)
                  )
          })
  @PostMapping
  public ResponseEntity<TokenAnswerDTO> login(@RequestBody AuthenticateUserDTO dto) {
    if (!authService.validateInputEmail(dto.getEmail()))
      return ResponseEntity.badRequest().body(new TokenAnswerDTO(false, null));

    if (!authService.validatePassword(dto))
      return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new TokenAnswerDTO(false, null));

    final Long userId = authService.findUserIdByEmail(dto.getEmail());

    final String JWT = jwtUtil.createUserToken(userId);

        /*
        Cookie cookie = new Cookie("Cookie-served-with-love", JWT);
        cookie.setMaxAge(7*24*60*60); // Set the expiry to one full week
        cookie.setHttpOnly(true);
        cookie.setPath("/api");
        response.addCookie(cookie);
        */

    return ResponseEntity.ok(new TokenAnswerDTO(true, JWT));
  }

  @Operation(summary = "Create an anonymous user", description = "Create an anonymous user")
  @ApiResponses(value = {
          @ApiResponse(
                  responseCode = "200",
                  description = "Anonymous user created",
                  content = {
                          @Content(mediaType = "application/json",
                                  schema = @Schema(implementation = TokenAnswerDTO.class)),
                  }),
  })
  @PostMapping("/anon")
  public ResponseEntity<TokenAnswerDTO> createAnonymousUser() {
    if (anonymousIdCounter == Long.MAX_VALUE)
      anonymousIdCounter = 0;

    final String JWT = jwtUtil.createAnonymousToken(anonymousIdCounter++);

    return ResponseEntity.ok(new TokenAnswerDTO(true, JWT));
  }

  @GetMapping("/test")
  public String login() {
    return "Success";
  }


  @GetMapping
  public String getSalt() {
    return secrets.getSalt();
  }
}
