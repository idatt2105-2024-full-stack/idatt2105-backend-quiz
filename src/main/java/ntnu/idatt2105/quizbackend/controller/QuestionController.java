package ntnu.idatt2105.quizbackend.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import ntnu.idatt2105.quizbackend.dto.QuestionDTO;
import ntnu.idatt2105.quizbackend.dto.QuestionQueryDTO;
import ntnu.idatt2105.quizbackend.entity.Question;
import ntnu.idatt2105.quizbackend.service.QuestionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

/**
 * The QuestionController is a controller that handles HTTP requests related to the Question entity
 * It is responsible for creating new questions and getting questions by the currently logged in user
 * The controller also has a method for getting a question by its id
 *
 * @RestController The Lombok annotation tells Spring that this class is a controller
 * @CrossOrigin The Lombok annotation tells Spring to allow requests from any origin
 * @RequestMapping The Lombok annotation is used to map web requests to specific handler classes and/or handler methods
 * @AllArgsConstructor The Lombok annotation generates a constructor with all arguments
 */

@RestController
@CrossOrigin
@RequestMapping("/api")
@AllArgsConstructor
public class QuestionController {

  /**
   * The QuestionService is a service that handles the business logic for the Question entity
   */
  private QuestionService questionService;

  @Operation(
          summary = "Create new questions",
          description = "Create new questions with the given question and answers"
  )
  @ApiResponses(value = {
          @ApiResponse(
                  responseCode = "201",
                  description = "Questions created",
                  content = {
                          @io.swagger.v3.oas.annotations.media.Content(mediaType = "application/json")
                  }
          ),
          @ApiResponse(
                  responseCode = "400",
                  description = "Invalid input for question or invalid question format",
                  content = @Content
          ),
          @ApiResponse(
                  responseCode = "400",
                  description = "Invalid input for answers or invalid answer format",
                  content = @Content
          ),
  })
  @Parameter(
          name = "QuestionDTO",
          description = "The question to be created",
          content = {
                  @Content(mediaType = "application/json",
                          schema = @Schema(implementation = QuestionDTO.class))
          })
  @PostMapping("/user/question")
  public ResponseEntity<List<QuestionDTO>> createNewQuestions(@RequestBody List<QuestionDTO> questions) {
    String error = null;

    for (QuestionDTO q : questions)
      if (error == null)
        error = validateQuestionDTO(q);

    if (error != null)
      return ResponseEntity.badRequest().build();

    List<Question> q = questionService.createNewQuestions(questions);
    return ResponseEntity.created(URI.create("/user/question")).body(q.stream().map(QuestionService::convertToQuestionDTO).toList());
  }


  @Operation(
          summary = "Get all questions by user",
          description = "Get all questions created by the currently logged in user"
  )
  @ApiResponses(value = {
          @ApiResponse(
                  responseCode = "401",
                  description = "Unauthorized access, client does not have a valid token",
                  content = @Content
          ),
  })
  @GetMapping("/user/question")
  public List<QuestionDTO> getUserQuestions() {
    return questionService.getAllQuestionsByUser()
            .stream()
            .map(QuestionService::convertToQuestionDTO)
            .toList();
  }


  @Operation(
          summary = "Get question",
          description = "Get a question by its id"
  )
  @ApiResponses(value = {
          @ApiResponse(
                  responseCode = "200",
                  description = "Question found",
                  content = {
                          @Content(mediaType = "application/json")
                  }
          ),
          @ApiResponse(
                  responseCode = "204",
                  description = "No question found",
                  content = @Content
          )
  })
  @Parameter(
          name = "QuestionQueryDTO",
          description = "The question to be found",
          content = {
                  @Content(mediaType = "application/json",
                          schema = @Schema(implementation = QuestionQueryDTO.class))
          })
  @GetMapping("/question")
  public ResponseEntity<QuestionDTO> getQuestion(@RequestBody QuestionQueryDTO dto) {
    Question q = questionService.getQuestion(dto.getId());

    if (q == null)
      return ResponseEntity.noContent().build();

    return ResponseEntity.ok().body(QuestionService.convertToQuestionDTO(q));
  }

  /**
   * Validates the contents of a QuestionDTO
   *
   * @param dto The QuestionDTO to validate
   * @return null if the DTO is valid, otherwise an error message
   * @see QuestionDTO
   */
  private String validateQuestionDTO(QuestionDTO dto) {
    if (dto.getQuestion() == null || dto.getQuestion().isBlank())
      return "Cannot register an empty question";

    if (dto.getPossibleAnswers() == null || dto.getPossibleAnswers().size() < 2)
      return "All questions must have at least 2 answers";

    return null;
  }
}
