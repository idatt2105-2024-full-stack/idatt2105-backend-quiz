package ntnu.idatt2105.quizbackend.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.AllArgsConstructor;
import ntnu.idatt2105.quizbackend.dto.TagDTO;
import ntnu.idatt2105.quizbackend.entity.Tag;
import ntnu.idatt2105.quizbackend.service.TagService;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Controller for handling tag related requests
 *
 * @RestController annotation tells Spring that this class will be used to handle incoming requests
 * @AllArgsConstructor annotation is used to create a constructor with all required arguments
 */
@RestController
@CrossOrigin
@AllArgsConstructor
public class TagController {

  /**
   * TagService class is used to handle the business logic for tags
   */
  private final TagService tagService;

  /**
   * ModelMapper class is used to map objects to other objects, for example from entity objects to DTOs
   */
  private ModelMapper mapper;

  @Operation(
          summary = "Get all tags for a quiz"
  )
  @ApiResponse(
          responseCode = "200",
          description = "Successfully retrieved tags",
          content = {@Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = TagDTO.class)
          )}
  )
  @Parameter(
          name = "quizId",
          description = "The id of the quiz",
          required = true,
          content = {@Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = Long.class)
          )}
  )
  @GetMapping("/api/tag/quiz")
  public ResponseEntity<List<TagDTO>> getQuizTags(@RequestBody Long quizId) {
    List<Tag> tags = tagService.getQuizTags(quizId);

    List<TagDTO> tagDTOs = tags.stream()
            .map(this::mapToDTO)
            .toList();

    return ResponseEntity.ok().body(tagDTOs);
  }


  @Operation(
          summary = "Get all tags for a question"
  )
  @ApiResponse(
          responseCode = "200",
          description = "Successfully retrieved tags",
          content = {@Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = TagDTO.class)
          )}
  )
  @GetMapping("/api/tag/question")
  public ResponseEntity<List<TagDTO>> getQuestionTags(@RequestBody Long questionId) {
    List<Tag> tags = tagService.getQuestionTags(questionId);

    List<TagDTO> tagDTOs = tags.stream()
            .map(this::mapToDTO)
            .toList();

    return ResponseEntity.ok().body(tagDTOs);
  }


  @Operation(
          summary = "Get all tags"
  )
  @ApiResponse(
          responseCode = "200",
          description = "Successfully retrieved tags",
          content = {@Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = TagDTO.class)
          )}
  )
  @GetMapping("/api/tag")
  public ResponseEntity<List<TagDTO>> getAllTags() {

    List<Tag> tags = tagService.getAllTags();

    List<TagDTO> tagDTOs = tags.stream()
            .map(this::mapToDTO)
            .toList();

    return ResponseEntity.ok().body(tagDTOs);
  }

  /**
   * Method for mapping a Tag entity to a TagDTO
   *
   * @param tag the tag entity
   * @return a TagDTO
   */
  public TagDTO mapToDTO(Tag tag) {
    return mapper.map(tag, TagDTO.class);
  }
}
