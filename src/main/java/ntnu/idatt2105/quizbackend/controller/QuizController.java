package ntnu.idatt2105.quizbackend.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import ntnu.idatt2105.quizbackend.dto.*;
import ntnu.idatt2105.quizbackend.entity.Question;
import ntnu.idatt2105.quizbackend.entity.Quiz;
import ntnu.idatt2105.quizbackend.entity.Tag;
import ntnu.idatt2105.quizbackend.repository.TagRepository;
import ntnu.idatt2105.quizbackend.repository.UserRepository;
import ntnu.idatt2105.quizbackend.security.SecurityConfig;
import ntnu.idatt2105.quizbackend.service.QuestionService;
import ntnu.idatt2105.quizbackend.service.QuizService;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for handling quiz related requests
 *
 * @RestController annotation tells Spring that this class will be used to handle incoming requests
 * @CrossOrigin annotation is used to handle the response headers to allow the client to access the response
 * @RequestMapping annotation is used to map web requests to specific handler classes or methods
 * @AllArgsConstructor annotation is used to create a constructor with all required arguments
 */
@RestController
@CrossOrigin
@RequestMapping("/api/quiz")
@AllArgsConstructor
public class QuizController {

  /**
   * QuizService class is used to handle the business logic for quizzes
   */
  private QuizService quizService;

  /**
   * QuestionService class is used to handle the database operations for questions
   */
  private QuestionService questionService;

  /**
   * TagRepository class is used to handle the database operations for tags
   */
  private TagRepository tagRepository;

  /**
   * UserRepository class is used to handle the database operations for users
   */
  private UserRepository userRepository;


  /**
   * ModelMapper class is used to map objects to other objects
   */
  private ModelMapper mapper;


  @Operation(
          summary = "Create a new quiz",
          description = "Create a new quiz with the given data from the request body, and return the created quiz."
  )
  @ApiResponses(value = {
          @ApiResponse(
                  responseCode = "201",
                  description = "Created",
                  content = {@Content(mediaType = "application/json",
                          schema = @Schema(implementation = QuizDTO.class)
                  )}),
          @ApiResponse(
                  responseCode = "400",
                  description = "Bad request, the format or information of the request body quizDTO is invalid",
                  content = {@Content(mediaType = "application/json",
                          schema = @Schema(implementation = QuizDTO.class)
                  )}),
          @ApiResponse(
                  responseCode = "401",
                  description = "Unauthorized access, client does not have a valid token",
                  content = {@Content(mediaType = "application/json",
                          schema = @Schema(implementation = TokenAnswerDTO.class)
                  )}),
  })
  @Parameter(
          name = "quizDTO",
          description = "The quiz data transfer object",
          content = @Content(mediaType = "application/json",
                  schema = @Schema(implementation = QuizDTO.class)
          )
  )
  @PostMapping("/create")
  public ResponseEntity<QuizDTO> createQuiz(@RequestBody QuizDTO quizDTO) {

    // Map the DTO to an entity
    Quiz quiz = mapToEntity(quizDTO);
    List<Question> questions = questionService.getQuestions(quizDTO.getQuestion_ids());
    List<Tag> tags = tagRepository.findAllById(quizDTO.getTag_ids());
    Long userId = SecurityConfig.getCurrentUser();

    // Set the questions and tags on the quiz
    quiz.setQuestions(questions);
    quiz.setTags(tags);

    // Set the user as the creator of the quiz
    quiz.setCreator(userRepository.findUserById(userId));

    // Call the service to create the quiz
    Quiz quizDB = quizService.createQuiz(quiz);

    if (quizDB == null) {
      return ResponseEntity.badRequest().build();
    }

    // Map the entity to a DTO
    QuizDTO quizDTODB = mapToDTO(quizDB);

    return ResponseEntity.created(URI.create("api/user/quiz")).body(quizDTODB);
  }


  @Operation(
          summary = "Get a quizzes",
          description = "Get a quizzes in a paginated format, and return the quizzes."
  )
  @ApiResponses(value = {
          @ApiResponse(
                  responseCode = "200",
                  description = "OK",
                  content = {@Content(mediaType = "application/json",
                          schema = @Schema(implementation = QuizDTO.class)
                  )}),
          @ApiResponse(
                  responseCode = "400",
                  description = "Bad request, the format or information of the request body pageableDTO is invalid",
                  content = {@Content(mediaType = "application/json",
                          schema = @Schema(implementation = PageableDTO.class)
                  )}),
  })
  @Parameter(
          name = "pageableDTO",
          description = "The pageable data transfer object",
          content = @Content(mediaType = "application/json",
                  schema = @Schema(implementation = PageableDTO.class)
          )
  )
  @PostMapping("/get/paginated")
  public ResponseEntity<List<QuizDTO>> getQuizzesPaginated(@RequestBody PageableDTO pageableDTO) {
    // Get all quizzes
    if (pageableDTO.getPage() < 0 || pageableDTO.getSize() <= 0) {
      return ResponseEntity.badRequest().build();
    }

    List<Quiz> quizzes;
    List<Tag> tags;
    List<Long> tagIds;

    // Check if the request contains tag ids
    if (pageableDTO.getTagIds() != null) {
      tagIds = pageableDTO.getTagIds();
      tags = tagRepository.findAllById(tagIds);
      quizzes = quizService.getQuizzesByTagsPaginated(pageableDTO.getPage(), pageableDTO.getSize(), tags);
    } else {
      quizzes = quizService.getQuizzesPaginated(pageableDTO.getPage(), pageableDTO.getSize());
    }

    // Map the entities to DTOs
    List<QuizDTO> quizDTOs = quizzes.stream()
            .map(this::mapToDTO)
            .toList();

    return ResponseEntity.ok().body(quizDTOs);
  }

  @Operation(
          summary = "Get a random quizzes",
          description = "Get a random quizzes in a paginated format, and return the quizzes."
  )
  @ApiResponses(value = {
          @ApiResponse(
                  responseCode = "200",
                  description = "OK",
                  content = {@Content(mediaType = "application/json",
                          schema = @Schema(implementation = QuizDTO.class)
                  )}),
          @ApiResponse(
                  responseCode = "204",
                  description = "No content, no quizzes found",
                  content = @Content
          ),
  })
  @GetMapping("/get/random/paginated")
  public ResponseEntity<List<QuizDTO>> getRandomQuizzesPaginated() {

    // Get six random quizzes
    List<Quiz> quizzes = quizService.getSixRandomQuizzes();

    // Map the entities to DTOs
    List<QuizDTO> quizDTOs = quizzes.stream()
            .map(this::mapToDTO)
            .toList();

    return ResponseEntity.ok(quizDTOs);
  }

  @Operation(
          summary = "Get a quizzes by user",
          description = "Get a quizzes by user in a paginated format, and return the quizzes."
  )
  @ApiResponses(value = {
          @ApiResponse(
                  responseCode = "200",
                  description = "OK",
                  content = {@Content(mediaType = "application/json",
                          schema = @Schema(implementation = QuizDTO.class)
                  )}),
          @ApiResponse(
                  responseCode = "400",
                  description = "Bad request, the format or information of the request body pageableDTO is invalid",
                  content = {@Content(mediaType = "application/json",
                          schema = @Schema(implementation = PageableDTO.class)
                  )}),
  })
  @PostMapping("/get/paginated/user")
  public ResponseEntity<List<QuizDTO>> getUserQuizzesPaginated(@RequestBody PageableDTO pageableDTO) {
    Long userId = SecurityConfig.getCurrentUser();

    List<Quiz> quizzes;
    List<Tag> tags;
    List<Long> tagIds;

    // Check if the request contains tag ids
    if (pageableDTO.getTagIds() != null) {
      tagIds = pageableDTO.getTagIds();
      tags = tagRepository.findAllById(tagIds);
      quizzes = quizService.getQuizzesByUserAndTagsPaginated(pageableDTO.getPage(), pageableDTO.getSize(), userId, tags);
    } else {
      quizzes = quizService.getQuizzesByUserPaginated(pageableDTO.getPage(), pageableDTO.getSize(), userId);
    }
    List<QuizDTO> quizDTOs = quizzes.stream()
            .map(this::mapToDTO)
            .toList();

    return ResponseEntity.ok().body(quizDTOs);
  }

  @PostMapping
  public ResponseEntity<QuizDTO> getSingleQuizById(@RequestBody StartQuizDTO quizId) {
    Quiz q = quizService.getQuizById(Long.valueOf(quizId.getQuizId()));

    if (q == null)
      return ResponseEntity.notFound().build();

    QuizDTO dto = mapToDTO(q);
    return ResponseEntity.ok().body(dto);
  }

  /**
   * Maps a QuizDTO to a Quiz entity
   *
   * @param quizDTO the quiz data transfer object
   * @return Quiz the quiz entity
   */
  private Quiz mapToEntity(QuizDTO quizDTO) {
    Quiz quiz = mapper.map(quizDTO, Quiz.class);

    List<Long> questionIds = quizDTO.getQuestion_ids();
    List<Long> tagIds = quizDTO.getTag_ids();

    // Fetch questions and tags from the database
    List<Question> questions = questionService.getQuestions(questionIds);
    List<Tag> tags = tagRepository.findAllById(tagIds);

    Long userId = SecurityConfig.getCurrentUser();

    // Set the questions and tags on the quiz
    quiz.setQuestions(questions);
    quiz.setTags(tags);

    // Set the user as the creator of the quiz
    quiz.setCreator(userRepository.findUserById(userId));

    return quiz;
  }

  /**
   * Maps a Quiz entity to a QuizDTO
   *
   * @param quiz the quiz entity
   * @return QuizDTO the quiz data transfer object
   */
  private QuizDTO mapToDTO(Quiz quiz) {
    return mapper.map(quiz, QuizDTO.class);
  }
}
