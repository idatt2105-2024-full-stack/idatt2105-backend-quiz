package ntnu.idatt2105.quizbackend.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import ntnu.idatt2105.quizbackend.dto.UserDTO;
import ntnu.idatt2105.quizbackend.service.UserDetailsService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * The UserController class is a REST controller that handles HTTP requests related to the User entity.
 * It is responsible for handling requests related to the User entity, such as getting all user details.
 *
 * @CrossOrigin The annotation is used to handle the response headers that allow the frontend to access the backend.
 * @RestController The annotation is used to define a REST controller.
 * @RequestMapping The annotation is used to map web requests onto specific handler classes and/or handler methods.
 * @AllArgsConstructor The Lombok annotation that generates a constructor with all arguments.
 */
@CrossOrigin
@RestController
@RequestMapping("/api/user")
@AllArgsConstructor
public class UserController {

  /**
   * The UserDetailsService object that is used to access the UserDetailsService class.
   */
  private final UserDetailsService userService;

  @Operation(
          summary = "Get all user details"
  )
  @ApiResponses(value = {
          @ApiResponse(
                  responseCode = "200",
                  description = "Successfully retrieved all user details",
                  content = {@Content(
                          mediaType = "application/json",
                          schema = @Schema(implementation = UserDTO.class)
                  )}
          ),
          @ApiResponse(
                  responseCode = "401",
                  description = "Unauthorized access, user is not logged in"
          ),
  })
  @GetMapping
  public ResponseEntity<UserDTO> getAllUserDetails() {
    return ResponseEntity.ok().body(userService.getAllDetails());
  }
}