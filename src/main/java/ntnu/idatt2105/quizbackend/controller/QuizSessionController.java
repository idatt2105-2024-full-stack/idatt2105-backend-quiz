package ntnu.idatt2105.quizbackend.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import ntnu.idatt2105.quizbackend.dto.*;
import ntnu.idatt2105.quizbackend.service.QuizSessionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


/**
 * The QuizSessionController is a controller that handles the REST API endpoints for quiz sessions
 * It is responsible for starting a new session, getting the current question, submitting an answer and deleting the session
 *
 * @RestController The Lombok annotation tells Spring that this class is a controller
 * @CrossOrigin The Lombok annotation tells Spring that this controller allows Cross-Origin Resource Sharing (CORS)
 * @AllArgsConstructor The Lombok annotation generates a constructor with all arguments
 * @RequestMapping The Lombok annotation tells Spring that this controller will handle requests at the given path
 */
@RestController
@CrossOrigin
@AllArgsConstructor
@RequestMapping("/api/quiz/session")
public class QuizSessionController {

  private QuizSessionService sessionService;

  @Operation(
          summary = "Start a new quiz session",
          description = "Start a new quiz session with the given quiz id"
  )
  @ApiResponses(value = {
          @ApiResponse(
                  responseCode = "200",
                  description = "Successfully started a new session",
                  content = {@Content(
                          mediaType = "application/json",
                          schema = @Schema(implementation = QuestionDTO.class)
                  )}),
          @ApiResponse(
                  responseCode = "204",
                  description = "No content",
                  content = @Content
          ),
          @ApiResponse(
                  responseCode = "400",
                  description = "Bad request, the quiz dto is invalid or format is wrong",
                  content = @Content
          ),
  })
  @Parameter(
          name = "dto",
          description = "The quiz dto with the required information to start a session with",
          required = true
  )
  @PutMapping("/new")
  public ResponseEntity<QuestionDTO> startNewSession(@RequestBody StartQuizDTO dto) {
    if (!sessionService.startNewSession(dto.getQuizId()))
      return ResponseEntity.noContent().build();

    return getQuestion();
  }

  @Operation(
          summary = "Get the current question",
          description = "Get the current question in the quiz session"
  )
  @ApiResponses(value = {
          @ApiResponse(
                  responseCode = "200",
                  description = "Successfully retrieved the question",
                  content = {@Content(
                          mediaType = "application/json",
                          schema = @Schema(implementation = QuestionDTO.class)
                  )}),
          @ApiResponse(
                  responseCode = "410",
                  description = "Gone, the session has ended",
                  content = @Content
          ),
  })
  @GetMapping
  public ResponseEntity<QuestionDTO> getQuestion() {
    QuestionDTO questionDTO = sessionService.getQuestion();

    if (questionDTO == null)
      return ResponseEntity.status(HttpStatus.GONE).body(null);

    return ResponseEntity.ok(questionDTO);
  }

  @Operation(
          summary = "Submit an answer",
          description = "Submit an answer to the current question"
  )
  @ApiResponses(value = {
          @ApiResponse(
                  responseCode = "200",
                  description = "Successfully submitted the answer",
                  content = {@Content(
                          mediaType = "application/json",
                          schema = @Schema(implementation = SubmitAnswerResponseDTO.class)
                  )}),
          @ApiResponse(
                  responseCode = "410",
                  description = "Gone, the session has ended",
                  content = @Content
          ),
  })
  @Parameter(
          name = "dto",
          description = "The answer dto with the chosen answer",
          required = true
  )
  @PostMapping
  public ResponseEntity<SubmitAnswerResponseDTO> submitAnswer(@RequestBody SubmitAnswerDTO dto) {
    SubmitAnswerResponseDTO responseDTO = sessionService.submitAnswer(dto.getChosenAnswer());

    if (responseDTO == null)
      return ResponseEntity.status(HttpStatus.GONE).build();

    return ResponseEntity.ok().body(responseDTO);
  }

  @Operation(
          summary = "Delete the current session",
          description = "Delete the current session"
  )
  @ApiResponses(value = {
          @ApiResponse(
                  responseCode = "200",
                  description = "Successfully deleted the session",
                  content = @Content
          ),
          @ApiResponse(
                  responseCode = "410",
                  description = "Gone, the session has ended",
                  content = @Content
          ),
  })
  @DeleteMapping
  public ResponseEntity<String> deleteSession() {
    if (!sessionService.deleteSession())
      return ResponseEntity.status(HttpStatus.GONE).build();

    return ResponseEntity.ok().build();
  }
}
