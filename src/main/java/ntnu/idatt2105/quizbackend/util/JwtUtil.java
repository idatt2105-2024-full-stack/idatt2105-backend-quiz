package ntnu.idatt2105.quizbackend.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import ntnu.idatt2105.quizbackend.security.SecretsConfig;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import java.time.Instant;

/**
 * The JwtUtil class is used to create and decode JWT tokens.
 * It is used to create tokens for users and anonymous users.
 *
 * @Component is used to indicate that the class is a Spring component.
 * @EnableConfigurationProperties is used to enable configuration properties.
 */
@Component
@EnableConfigurationProperties(SecretsConfig.class)
public class JwtUtil {

    /**
     * The time in milliseconds that a token is valid for.
     */
    private static final long TOKEN_EXPIRY_TIME = 18*60*60_000;

    /**
     * The Algorithm object is used to encode and decode JWT tokens.
     */
    private final Algorithm encoder;

    /**
     * The JWTVerifier object is used to verify JWT tokens.
     */
    private final JWTVerifier verifier;

    /**
     * Constructor for the JwtUtil class.
     *
     * @param secrets is a SecretsConfig object
     */
    public JwtUtil(SecretsConfig secrets) {
        encoder = Algorithm.HMAC512(secrets.getJwt());
        verifier = JWT.require(encoder).build();
    }

    /**
     * This method is used to decode a JWT token.
     *
     * @param token is the token that will be decoded
     * @return the decoded JWT token
     */
    public DecodedJWT decodeToken(String token) {
        try {
            return verifier.verify(token);
        } catch (final JWTVerificationException verificationEx) {
            return null;
        }
    }

    /**
     * This method is used to create a JWT token for a user.
     *
     * @param username is the username of the user
     * @return the JWT token
     * @deprecated This method is deprecated and should not be used.
     */
    @Deprecated
    public String createToken(String username) {
        final Instant now = Instant.now();

        return JWT.create()
                .withSubject(username)
                .withIssuedAt(now)
                .withExpiresAt(now.plusMillis(TOKEN_EXPIRY_TIME))
                .sign(encoder);
    }

    /**
     * This method is used to create a JWT token for a user.
     *
     * @param userId is the id of the user
     * @return the JWT token
     */
    public String createUserToken(Long userId) {
        final Instant now = Instant.now();

        return JWT.create()
                .withSubject("ROLE_USER:" + userId.toString())
                .withIssuedAt(now)
                .withExpiresAt(now.plusMillis(TOKEN_EXPIRY_TIME))
                .sign(encoder);
    }

    /**
     * This method is used to create a JWT token for an anonymous user.
     *
     * @param userId is the id of the user
     * @return the JWT token
     */
    public String createAnonymousToken(Long userId) {
        final Instant now = Instant.now();

        return JWT.create()
                .withSubject("ROLE_TEMPORARY:" + userId.toString())
                .withIssuedAt(now)
                .withExpiresAt(now.plusMillis(TOKEN_EXPIRY_TIME))
                .sign(encoder);
    }

    /**
     * This method is used to check if a token is expired.
     *
     * @param token is the token that will be checked
     * @return true if the token is expired, false otherwise
     */
    public boolean isTokenExpired(DecodedJWT token) {
        final Instant now = Instant.now();

        return now.isAfter(token.getExpiresAtAsInstant());
    }
}