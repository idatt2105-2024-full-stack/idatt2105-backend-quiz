package ntnu.idatt2105.quizbackend.util;

import ntnu.idatt2105.quizbackend.security.SecretsConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * The SaltUtil class is used to encrypt passwords.
 * It is used to encrypt passwords before they are stored in the database.
 *
 * @Component is used to indicate that the class is a Spring component.
 * @EnableConfigurationProperties is used to enable configuration properties.
 */
@Component
@EnableConfigurationProperties(SecretsConfig.class)
public class SaltUtil {

  /**
   * The SALT is a secret string that is used to encrypt passwords.
   */
  private final String SALT;

  /**
   * The MessageDigest object is used to encrypt passwords.
   */
  private final MessageDigest digest;

  /**
   * Constructor for the SaltUtil class.
   *
   * @param secrets is a SecretsConfig object
   * @throws NoSuchAlgorithmException is thrown if the algorithm is not found
   */
  @Autowired
  public SaltUtil(SecretsConfig secrets) throws NoSuchAlgorithmException {
    SALT = secrets.getSalt();

    digest = MessageDigest.getInstance("SHA-256");
  }
}
