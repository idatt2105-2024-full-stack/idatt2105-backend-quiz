package ntnu.idatt2105.quizbackend.service;

import lombok.AllArgsConstructor;
import ntnu.idatt2105.quizbackend.dto.UserDTO;
import ntnu.idatt2105.quizbackend.entity.User;
import ntnu.idatt2105.quizbackend.repository.UserRepository;
import ntnu.idatt2105.quizbackend.security.SecurityConfig;
import org.springframework.stereotype.Service;

/**
 * The UserDetailsService class is used to access the UserRepository, AttemptService, QuestionService and QuizService classes.
 *
 * @Service The annotation is used to indicate that the class is a service class.
 * @AllArgsConstructor The Lombok annotation that generates a constructor with all arguments.
 */
@Service
@AllArgsConstructor
public class UserDetailsService {

    /**
     * The UserRepository object that is used to access the UserRepository class.
     */
    private UserRepository dbUser;

    /**
     * The AttemptService object that is used to access the AttemptService class.
     */
    private AttemptService attemptService;

    /**
     * The QuestionService object that is used to access the QuestionService class.
     */
    private QuestionService questionService;

    /**
     * The QuizService object that is used to access the QuizService class.
     */
    private QuizService quizService;

    /**
     * The getAllDetails method is used to get all user details.
     *
     * @return A UserDTO object that contains all user details.
     */
    public UserDTO getAllDetails() {
        Long userId = SecurityConfig.getCurrentUser();


        UserDTO dto = new UserDTO();

        User userRecord = dbUser.findUserById(userId);
        dto.setEmail(userRecord.getEmail());
        dto.setUsername(userRecord.getUsername());

        dto.setAttempts(userRecord.getAttempts().stream().map(attemptService::convertToAttemptDTO).toList());
        dto.setQuestions(questionService.getAllQuestionsByUser().stream().map(QuestionService::convertToQuestionDTO).toList());
        dto.setQuizzes(quizService.getAllQuizzesByUser().stream().map(quizService::convertToQuizDTO).toList());

        return dto;
    }
}
