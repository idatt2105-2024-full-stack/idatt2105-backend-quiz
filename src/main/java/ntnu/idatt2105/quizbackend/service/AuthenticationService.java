package ntnu.idatt2105.quizbackend.service;

import lombok.AllArgsConstructor;
import ntnu.idatt2105.quizbackend.entity.User;
import ntnu.idatt2105.quizbackend.dto.AuthenticateUserDTO;
import ntnu.idatt2105.quizbackend.dto.NewUserDTO;
import ntnu.idatt2105.quizbackend.repository.UserRepository;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.stereotype.Service;

/**
 * Service class for the authentication of users.
 * The class is used to register new users, validate passwords, and check if a user exists in the database.
 *
 * @Service The Lombok annotation Service is used to indicate that the class is a service class.
 * @AllArgsConstructor The Lombok annotation AllArgsConstructor generates a constructor with all the arguments.
 */
@Service
@AllArgsConstructor
public class AuthenticationService {

    /**
     * The UserRepository object is used to access the methods in the UserRepository class.
     * It is used to interact with the database.
     */
    private UserRepository database;

    /**
     * Registers a new user in the database. Returns the ID of the created entity for future reference.
     *
     * @param dto The DTO containing the information of the new user.
     * @return The user ID of the newly registered user.
     * @see NewUserDTO
     */
    public Long registerNewUser(NewUserDTO dto) {
        User newUser = dto.toNewUserEntry();

        return database.save(newUser).getId();
    }

    /**
     * Validates the password of a user.
     *
     * @param dto The DTO containing the email and password of the user.
     * @return True if the password is correct, false otherwise.
     * @see AuthenticateUserDTO
     */
    public boolean validatePassword(AuthenticateUserDTO dto) {
        String submittedPassword = dto.getPassword();
        String storedPassword = database.findPasswordByEmail(dto.getEmail());

        return submittedPassword.equals(storedPassword);
    }

    /**
     * Returns the user ID of a user with the given email.
     *
     * @param email The email of the user.
     * @return The user ID of the user with the given email.
     */
    public Long findUserIdByEmail(String email) {
        return database.findUserIdByEmail(email);
    }

    /**
     * Returns if the user with the given email exists in the database.
     *
     * @param email The email of the user.
     * @return True if the user exists, false otherwise.
     */
    public boolean userExists(String email) {
        return database.existsByEmail(email);
    }

    /**
     * Validates the input email.
     *
     * @param email The email to validate.
     * @return True if the email is valid, false otherwise.
     */
    public boolean validateInputEmail(String email) {
        return EmailValidator.getInstance().isValid(email);
    }
}
