package ntnu.idatt2105.quizbackend.service;

import ntnu.idatt2105.quizbackend.dto.QuestionDTO;
import ntnu.idatt2105.quizbackend.dto.SubmitAnswerResponseDTO;
import ntnu.idatt2105.quizbackend.entity.Question;
import ntnu.idatt2105.quizbackend.entity.Quiz;
import ntnu.idatt2105.quizbackend.repository.UserRepository;
import ntnu.idatt2105.quizbackend.security.SecurityConfig;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;

import static ntnu.idatt2105.quizbackend.service.QuestionService.convertToQuestionDTO;

/**
 * Abstracts away the user-based sorting of all actions related to quiz sessions and quiz attempts.
 * All methods choose the correct user based on the current client's authentication from
 * the active SecurityContextHolder.
 *
 * @Service The Lombok annotation tells Spring that this class is a service
 */
@Service
public class QuizSessionService {

    /**
     * The QuizSessionManager is a class that manages all the quiz sessions for a user.
     */
    private final QuizSessionManager userSessions;

    /**
     * The QuizSessionManager is a class that manages all the quiz sessions for an anonymous user.
     */
    private final QuizSessionManager anonSessions;

    /**
     * The QuizService is a service that handles the business logic for quizzes.
     */
    private final QuizService dbQuiz;

    /**
     * The constructor for the QuizSessionService class.
     *
     * @param dbQuiz The QuizService that handles the business logic for quizzes.
     * @param attemptService The AttemptService that handles the business logic for attempts.
     * @param dbUser The UserRepository that handles the database operations for users.
     */
    public QuizSessionService(QuizService dbQuiz, AttemptService attemptService, UserRepository dbUser) {
        this.dbQuiz = dbQuiz;

        userSessions = new QuizSessionManager(attemptService, dbUser);
        anonSessions = new QuizSessionManager(attemptService, dbUser);
    }

    /**
     * Initiates a new Quiz session, replacing/overwriting the current session.
     *
     * @param quizId The ID of the quiz to create a new quiz session of out.
     * @return true if the requested quiz was found and a session was started. Otherwise, returns false.
     */
    public boolean startNewSession(long quizId) {
        Long userId = SecurityConfig.getCurrentUser();
        QuizSessionManager sessions = SecurityConfig.isUser() ? userSessions : anonSessions;

        Quiz quiz = dbQuiz.getQuizById(quizId);
        if (quiz == null)
            return false;

        sessions.startNewQuizGame(userId, quiz);
        return true;
    }

    /**
     * Gets the current question of the quiz session.
     *
     * @return QuestionDTO if a session was found, and the current question was retrieved. Otherwise, returns null.
     */
    public QuestionDTO getQuestion() {
        Long userId = SecurityConfig.getCurrentUser();
        QuizSessionManager sessions = SecurityConfig.isUser() ? userSessions : anonSessions;

        QuizGame quizGame = sessions.getQuizGame(userId);
        if (quizGame == null)
            return null;

        Question questionEntity = quizGame.getCurrentQuestion();
        if (questionEntity == null)
            return null;

        return convertToQuestionDTO(questionEntity);
    }

    /**
     * Submits an answer to the current question, and moves the quiz to the next question.
     *
     * @param answer The index of the chosen answer in the question's array of answers.
     * @return SubmitAnswerResponseDTO if a session was found, and the answer was submitted. Otherwise, returns null.
     */
    public SubmitAnswerResponseDTO submitAnswer(int answer) {
        Long userId = SecurityConfig.getCurrentUser();
        boolean isUser = SecurityConfig.isUser();
        QuizSessionManager sessions = isUser ? userSessions : anonSessions;

        // Submit answer, and receive DTO
        SubmitAnswerResponseDTO responseDTO = sessions.submitAnswer(userId, answer);

        // If there was no response, that means there are no sessions for this user.
        if (responseDTO == null)
            return null; // In those cases, we should return null

        // If this was the last question...
        if (responseDTO.getNext() == null) {
            if (isUser) // if the sessions belong to a registered user
                sessions.storeAttempt(userId); // store the user

            sessions.deleteSession(userId); // delete the session from memory
        }

        return responseDTO;
    }

    /**
     * Deletes the currently running quiz session. If a session was found - and subsequently deleted - this function
     * returns true. If no session was found or deleted, this function returns false.
     *
     * @return True if a session was deleted, or false if no session could be found.
     */
    @DeleteMapping
    public boolean deleteSession() {
        Long userId = SecurityConfig.getCurrentUser();
        QuizSessionManager sessions = SecurityConfig.isUser() ? userSessions : anonSessions;

        return sessions.deleteSession(userId) != null;
    }
}
