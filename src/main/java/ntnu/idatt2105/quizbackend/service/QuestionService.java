package ntnu.idatt2105.quizbackend.service;

import lombok.AllArgsConstructor;
import lombok.NonNull;
import ntnu.idatt2105.quizbackend.dto.AnswerDTO;
import ntnu.idatt2105.quizbackend.dto.QuestionDTO;
import ntnu.idatt2105.quizbackend.entity.Answer;
import ntnu.idatt2105.quizbackend.entity.Question;
import ntnu.idatt2105.quizbackend.entity.Tag;
import ntnu.idatt2105.quizbackend.repository.QuestionRepository;
import ntnu.idatt2105.quizbackend.repository.TagRepository;
import ntnu.idatt2105.quizbackend.repository.UserRepository;
import ntnu.idatt2105.quizbackend.security.SecurityConfig;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * The QuestionService is a service that handles the business logic for the Question entity
 * It is responsible for creating new questions and getting questions by the currently logged in user
 * The service also has a method for getting a question by its id
 *
 * @Service The Lombok annotation tells Spring that this class is a service
 * @AllArgsConstructor The Lombok annotation generates a constructor with all arguments
 */
@Service
@AllArgsConstructor
public class QuestionService {

  /**
   * The QuestionRepository is a repository that handles the database operations for the Question entity
   */
  private QuestionRepository dbQuestion;

  /**
   * The UserRepository is a repository that handles the database operations for the User entity
   */
  private UserRepository dbUser;

  /**
   * The TagRepository is a repository that handles the database operations for the Tag entity
   */
  private TagRepository dbTag;

  /**
   * Stores a new question in the database. The parameter 'dto' is a Data Transfer Object received from a client.
   * This function does not perform any input validation of the DTOs fields. It simply makes sure that
   * whatever the DTO contains is stored in the database.
   *
   * @param dto A list of The Data Transfer Object containing details about the question to be stored.
   * @return The resulting entities that is stored in the database as a list.
   * @throws IllegalArgumentException if 'dto' is null
   */
  public List<Question> createNewQuestions(@NonNull List<QuestionDTO> dto) {
    // Convert dto to entity
    List<Question> questions = dto.stream().map(this::toQuestionEntity).toList();

    // Write the new question to the database, and return the resulting entity
    return dbQuestion.saveAll(questions);
  }

  /**
   * Returns a question by its id.
   *
   * @param id The id of the question to be returned.
   * @return The question with the given id.
   */
  public Question getQuestion(Long id) {
    return dbQuestion.findQuestionById(id);
  }

  /**
   * Returns a list of questions by their ids.
   *
   * @param ids The ids of the questions to be returned.
   * @return The questions with the given ids.
   */
  public List<Question> getQuestions(List<Long> ids) {
    return dbQuestion.findAllById(ids);
  }

  /**
   * Returns all questions created by the currently logged in user.
   * This function uses the stored authentication in the current SecurityContextHolder
   * as the source for finding the correct user.
   *
   * @return All questions created by the currently logged in user.
   */
  public List<Question> getAllQuestionsByUser() {
    long userId = SecurityConfig.getCurrentUser();

    return dbQuestion.findQuestionsByCreatorId(userId);
  }

  /**
   * Converts a Question entity to a QuestionDTO
   *
   * @param q The Question entity to convert
   * @return The converted QuestionDTO
   * @see QuestionDTO
   */
  public static QuestionDTO convertToQuestionDTO(@NonNull Question q) {
    QuestionDTO dto = new QuestionDTO();

    dto.setId(q.getId());
    dto.setQuestion(q.getText());
    if (q.getTags() != null)
      dto.setTag_ids(q.getTags().stream().map(Tag::getId).toList());
    dto.setContentURI(q.getResource_url());
    dto.setPossibleAnswers(q.getAnswers().stream().map(QuestionService::convertToAnswerDTO).toList());

    return dto;
  }

  /**
   * Converts an Answer entity to an AnswerDTO
   *
   * @param a The Answer entity to convert
   * @return The converted AnswerDTO
   * @see AnswerDTO
   */
  public static AnswerDTO convertToAnswerDTO(@NonNull Answer a) {
    AnswerDTO dto = new AnswerDTO();
    dto.setAnswer(a.getText());
    dto.setCorrect(a.isCorrectAnswer());
    return dto;
  }

  /**
   * Converts a QuestionDTO to a Question entity
   *
   * @param dto The QuestionDTO to convert
   * @return The converted Question entity
   * @see Question
   */
  public Question toQuestionEntity(@NonNull QuestionDTO dto) {
    // Setup new entity for the new question
    Question q = new Question();

    q.setText(dto.getQuestion()); // Add the question text
    q.setResource_url(dto.getContentURI()); // Add the question resource
    q.setCreator(dbUser.findUserById(SecurityConfig.getCurrentUser())); // Add the current user as the creator of the question
    if (dto.getTag_ids() != null)
      q.setTags(dbTag.findAllById(dto.getTag_ids())); // Add any tags that the question should be marked with

    // Move all answers from the DTO to the new entity
    List<Answer> answers = new ArrayList<>();
    for (AnswerDTO a : dto.getPossibleAnswers()) {
      Answer newAnswer = new Answer();
      newAnswer.setQuestion(q);
      newAnswer.setText(a.getAnswer());
      newAnswer.setCorrectAnswer(a.isCorrect());
      answers.add(newAnswer);
    }
    q.setAnswers(answers);

    return q;
  }
}
