package ntnu.idatt2105.quizbackend.service;

import lombok.Getter;
import lombok.Setter;
import ntnu.idatt2105.quizbackend.dto.QuestionDTO;
import ntnu.idatt2105.quizbackend.dto.SubmitAnswerResponseDTO;
import ntnu.idatt2105.quizbackend.entity.Question;
import ntnu.idatt2105.quizbackend.entity.Quiz;
import ntnu.idatt2105.quizbackend.repository.UserRepository;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * In this class, the words 'quiz' and 'session' is used interchangeably. A session should be thought of as an
 * instance of QuizGame that is stored withing this class in the field 'activeSessions'.
 */
public class QuizSessionManager {

    /**
     * The activeSessions field is a map that stores all active quiz sessions. The key is the user ID of the user
     * that started the session, and the value is the QuizGame instance that represents the session.
     */
    private final Map<Long, QuizGame> activeSessions = new HashMap<>();

    /**
     * The AttemptService is a service that handles the business logic for attempts.
     */
    private final AttemptService attemptService;

    /**
     * The UserRepository is a repository that handles the database operations for users.
     */
    private final UserRepository dbUser;

    /**
     * The maxSessionAge field is the maximum age a session can have before it is deleted.
     * @Getter The Lombok annotation generates a getter for the field.
     * @Setter The Lombok annotation generates a setter for the field.
     */
    @Getter
    @Setter
    private long maxSessionAge = 2 * 24 * 3600 * 1000; // Default: 2 days

    /**
     * The cleanupInterval field is the interval at which the sessions are cleaned up.
     * @Getter The Lombok annotation generates a getter for the field.
     * @Setter The Lombok annotation generates a setter for the field.
     */
    @Getter
    @Setter
    private long cleanupInterval = 3600 * 1000; // Default: every hour

    /**
     * The lastCleanup field is the time of the last cleanup.
     */
    private long lastCleanup;

    /**
     * The constructor for the QuizSessionManager class.
     *
     * @param attemptService The AttemptService that handles the business logic for attempts.
     * @param dbUser The UserRepository that handles the database operations for users.
     */
    public QuizSessionManager(AttemptService attemptService, UserRepository dbUser) {
        this.attemptService = attemptService;
        this.dbUser = dbUser;
    }

    /**
     * Gets the current quiz session of the given user.
     *
     * @param user The user ID of the selected user.
     * @return The QuizGame instance that represents the session.
     */
    public QuizGame getQuizGame(Long user) {
        return activeSessions.get(user);
    }

    /**
     * Initiates a new Quiz session, replacing/overwriting the current session for the given user.
     *
     * @param userId The ID of the user to start a new session for.
     * @param quizEntity The quiz to create a new quiz session of out.
     */
    public void startNewQuizGame(Long userId, Quiz quizEntity) {
        // Silently perform cleanup in intervals
        if (System.currentTimeMillis() - lastCleanup >= cleanupInterval) {
            cleanupSessions(maxSessionAge);
            lastCleanup = System.currentTimeMillis();
        }

        // Create a new instance
        QuizGame game = new QuizGame(quizEntity);

        // Register the new session
        activeSessions.put(userId, game);
    }

    /**
     * Submits an answer to the chosen user's quiz session, and moves the quiz session to the next question.
     *
     * @param user The user ID of the selected user.
     * @param answer The index of the chosen answer in the question's array of answers.
     * @return A data transfer object to be sent as an answer back to the client.
     */
    public SubmitAnswerResponseDTO submitAnswer(Long user, int answer) {
        // Extract correct session
        QuizGame session = activeSessions.get(user);

        // Skip if the user has no session
        if (session == null)
            return null;

        // Keep last score
        int lastScore = session.getScore();

        // Submit answer
        session.submitAnswerAndGotoNextQuestion(answer);

        // Calculate gain
        int gain = session.getScore() - lastScore;

        // Get next question
        Question nextQuestion = session.getCurrentQuestion();

        QuestionDTO questionDTO = null;
        if (nextQuestion != null)
            questionDTO = QuestionService.convertToQuestionDTO(nextQuestion);

        // Deliver the response DTO
        return new SubmitAnswerResponseDTO(
                session.getScore(),
                gain,
                questionDTO
        );
    }

    /**
     * Stores a user's quiz session as an Attempt in the database only if the quiz has been completed.
     *
     * @param user The user ID of the user which session should be stored.
     */
    public void storeAttempt(Long user) {
        QuizGame session = activeSessions.get(user);

        // If the quiz has been finished
        if (session.completed()) {
            session.getAttemptEntity().setAttempter(dbUser.findUserById(user));
            attemptService.storeAttempt(session.getAttemptEntity()); // store the attempt
        }
    }

    /**
     * Deletes the session of the given user.
     *
     * @param userId The ID of the user to delete the session of.
     * @return The QuizGame instance that represents the session.
     */
    public QuizGame deleteSession(Long userId) {
        return activeSessions.remove(userId);
    }

    /**
     * Deletes all
     *
     * @param maxSessionLifetime The age threshold to use.
     */
    public void cleanupSessions(long maxSessionLifetime) {
        if (maxSessionLifetime < 0)
            throw new IllegalArgumentException("Parameter 'maxSessionLifetime' in QuizSessionManager::cleanupSessions(long) must be positive");

        // Define the threshold date
        Date threshold = new Date(System.currentTimeMillis() - maxSessionLifetime);

        // Delete all sessions that started before the threshold date
        activeSessions.values().removeIf((QuizGame q) -> q.getStartDate().before(threshold));
    }
}
