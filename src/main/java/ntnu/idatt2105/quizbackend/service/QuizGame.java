package ntnu.idatt2105.quizbackend.service;

import lombok.Getter;
import ntnu.idatt2105.quizbackend.entity.Answer;
import ntnu.idatt2105.quizbackend.entity.Attempt;
import ntnu.idatt2105.quizbackend.entity.Question;
import ntnu.idatt2105.quizbackend.entity.Quiz;

import java.util.Date;

/**
 * The QuizGame class is responsible for handling the game logic of a quiz.
 * It is responsible for keeping track of the current question, the score, and the streak multiplier.
 */
public class QuizGame {

  /**
   * The score that the user gets for each correct answer.
   */
  private static final int SCORE_PER_QUESTION = 100;

  /**
   * The quiz that this game is based on.
   */
  private final Quiz entity;

  /**
   * The attempt that is created when the quiz is completed.
   */
  private final Attempt attempt = new Attempt();

  /**
   * The index of the current question.
   */
  private int currentQuestion = 0;

  /**
   * The score of the user.
   *
   * @Getter is used to automatically generate a getter for the field.
   */
  @Getter
  private int score = 0;

  /**
   * The streak multiplier that is used to calculate the score.
   *
   * @Getter is used to automatically generate a getter for the field.
   */
  @Getter
  private float streakMultiplier = 1.0f;

  /**
   * Constructor for the QuizGame class.
   *
   * @param quiz The quiz that the game is based on.
   */
  public QuizGame(Quiz quiz) {
    this.entity = quiz;
    attempt.setDate(new Date());
    attempt.setQuiz(quiz);
  }

  /**
   * Returns the current question to be answered. The user of this class is expected to choose an
   * answer among the array of answers in the returned object.
   * To submit an answer, see QuizGame::submitAnswerAndGotoNextQuestion().
   *
   * @return the current question to be answered, or null if the quiz has been finished.
   */
  public Question getCurrentQuestion() {
    if (completed())
      return null;
    return entity.getQuestions().get(currentQuestion);
  }


  /**
   * Submits an answer to the current question, and moves the quiz forward to the next question.
   *
   * @param answer The index of the chosen answer,
   *               chosen from the array of answers within QuizGame::getCurrentQuestion().
   * @return null if there are more questions, and if an Attempt-object if this was the last question,
   * marking the end of this quiz.
   */
  public Attempt submitAnswerAndGotoNextQuestion(int answer) {
    if (completed())
      return attempt;

    Answer chosenAnswer = getCurrentQuestion().getAnswers().get(answer);
    attempt.getChosen_answers().add(chosenAnswer);

    if (chosenAnswer.isCorrectAnswer()) {
      score += (int) (SCORE_PER_QUESTION * streakMultiplier);
      streakMultiplier *= 1.3f;
    } else
      streakMultiplier = 1.0f;

    ++currentQuestion;
    if (!completed())
      return null;

    attempt.setScore(getScore());

    return attempt;
  }

  /**
   * Indicates whether this quiz has been completed. All answers in the quiz must be answered for the quiz to
   * be regarded as completed.
   *
   * @return true if all questions have been answered. Otherwise, returns false.
   */
  public boolean completed() {
    return currentQuestion >= entity.getQuestions().size();
  }

  /**
   * Serves the attempt-object that was created when this quiz was completed.
   *
   * @return the Attempt-object if this quiz has been completed. Otherwise, returns null.
   */
  public Attempt getAttemptEntity() {
    if (!completed())
      return null;

    return attempt;
  }

  /**
   * Returns the start date of the quiz.
   *
   * @return the start date of the quiz.
   */
  public Date getStartDate() {
    return attempt.getDate();
  }
}
