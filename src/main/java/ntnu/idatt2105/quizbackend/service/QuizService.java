package ntnu.idatt2105.quizbackend.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import ntnu.idatt2105.quizbackend.dto.QuizDTO;
import ntnu.idatt2105.quizbackend.entity.Question;
import ntnu.idatt2105.quizbackend.entity.Quiz;
import ntnu.idatt2105.quizbackend.entity.Tag;
import ntnu.idatt2105.quizbackend.repository.QuizRepository;
import ntnu.idatt2105.quizbackend.security.SecurityConfig;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Service class for the Quiz entity
 *
 * @Service annotation is used to indicate that the class is a service class
 * @AllArgsConstructor is used to create a constructor with all class attributes
 * @see ntnu.idatt2105.quizbackend.entity.Quiz
 * @see ntnu.idatt2105.quizbackend.repository.QuizRepository
 */
@Service
@AllArgsConstructor
public class QuizService {

  /**
   * The repository for the Quiz entity
   * @see ntnu.idatt2105.quizbackend.repository.QuizRepository
   */
  private final QuizRepository quizRepository;

  /**
   * The service for the Question entity
   * @see ntnu.idatt2105.quizbackend.service.QuestionService
   */
  private final QuestionService questionService;

  /**
   * Method to create a quiz
   *
   * @param quiz the quiz to be created, the quiz is a Quiz object
   * @return the created quiz
   */
  public Quiz createQuiz(Quiz quiz) {

    // Save the quiz to the database
    return quizRepository.save(quiz);
  }

  /**
   * Method to get all quizzes in a paginated format
   *
   * @param page the page number, the page number is an int.
   * @param size the size of the page, the size is an int
   * @return a list of quizzes, the list is a List of Quiz objects
   * @see ntnu.idatt2105.quizbackend.entity.Quiz
   * @see ntnu.idatt2105.quizbackend.repository.QuizRepository
     */
  public List<Quiz> getQuizzesPaginated(int page, int size) {

    Pageable pageable = PageRequest.of(page, size, Sort.by("id").ascending());

    return quizRepository.findAll(pageable).stream().toList();
  }

  /**
   * Method to get six random quizzes in a paginated format
   *
   * @return a list of six random quizzes, the list is a List of Quiz objects
   * @see ntnu.idatt2105.quizbackend.entity.Quiz
   * @see ntnu.idatt2105.quizbackend.repository.QuizRepository
   * @see ntnu.idatt2105.quizbackend.repository.QuizRepository#findSixRandomQuizzes()
   */
  public List<Quiz> getSixRandomQuizzes() {
    return quizRepository.findSixRandomQuizzes();
  }

  /**
   * Method to get all quizzes
   *
   * @return a list of quizzes, the list is a List of Quiz objects
   * @see ntnu.idatt2105.quizbackend.entity.Quiz
   * @see ntnu.idatt2105.quizbackend.repository.QuizRepository
   */
  public List<Quiz> getQuizzes() {
    return quizRepository.findAll();
  }

  /**
   * Method to get quizzes by user in a paginated format
   *
   * @param page the page number, the page number is an int.
   * @param size the size of the page, the size is an int
   * @param userId the id of the user, the id is a Long
   * @return a list of quizzes, the list is a List of Quiz objects
   * @see ntnu.idatt2105.quizbackend.entity.Quiz
   * @see ntnu.idatt2105.quizbackend.repository.QuizRepository
   * @see ntnu.idatt2105.quizbackend.repository.QuizRepository#findAllByCreatorId(Long, Pageable)
   */
  public List<Quiz> getQuizzesByUserPaginated(int page, int size, Long userId) {

    Pageable pageable = PageRequest.of(page, size, Sort.by("id").ascending());

    return quizRepository.findAllByCreatorId(userId, pageable).stream().toList();
  }

  /**
   * Method to get quizzes by user and tags in a paginated format
   *
   * @param page the page number, the page number is an int.
   * @param size the size of the page, the size is an int
   * @param userId the id of the user, the id is a Long
   * @param tags the tags, the tags is a List of Tag objects
   * @return a list of quizzes, the list is a List of Quiz objects
   * @see ntnu.idatt2105.quizbackend.entity.Quiz
   * @see ntnu.idatt2105.quizbackend.entity.Tag
   * @see ntnu.idatt2105.quizbackend.repository.QuizRepository
   * @see ntnu.idatt2105.quizbackend.repository.QuizRepository#findAllByCreatorIdAndTags(Long, List, Pageable)
   */
  public List<Quiz> getQuizzesByUserAndTagsPaginated(int page, int size, Long userId, List<Tag> tags) {

      Pageable pageable = PageRequest.of(page, size, Sort.by("id").ascending());

      return quizRepository.findAllByCreatorIdAndTags(userId, tags, pageable).stream().toList();
  }

/**
   * Method to get quizzes by tags in a paginated format
   *
   * @param page the page number, the page number is an int.
   * @param size the size of the page, the size is an int
   * @param tags the tags, the tags is a List of Tag objects
   * @return a list of quizzes, the list is a List of Quiz objects
   * @see ntnu.idatt2105.quizbackend.entity.Quiz
   * @see ntnu.idatt2105.quizbackend.entity.Tag
   * @see ntnu.idatt2105.quizbackend.repository.QuizRepository
   * @see ntnu.idatt2105.quizbackend.repository.QuizRepository#findAllByTags(List, Pageable)
   */
  public List<Quiz> getQuizzesByTagsPaginated(int page, int size, List<Tag> tags) {

    Pageable pageable = PageRequest.of(page, size, Sort.by("id").descending());

    return quizRepository.findAllByTags(tags, pageable).stream().toList();
  }

  /**
   * Method to get a quiz by its id
   *
   * @param id the id of the quiz, the id is a long
   * @return the quiz with the given id
   * @see ntnu.idatt2105.quizbackend.entity.Quiz
   * @see ntnu.idatt2105.quizbackend.repository.QuizRepository
   * @see ntnu.idatt2105.quizbackend.service.QuestionService
   * @see ntnu.idatt2105.quizbackend.repository.QuizRepository#findQuizById(Long)
   * @see ntnu.idatt2105.quizbackend.service.QuestionService#getQuestion(Long)
   */
  public Quiz getQuizById(Long id) {
    Quiz quiz = quizRepository.findQuizById(id);
    if (quiz == null)
      return null;

    List<Question> questions = new ArrayList<>();
    for (Question q : quiz.getQuestions()) {
      q = questionService.getQuestion(q.getId());
      questions.add(q);
    }

    quiz.setQuestions(questions);

    return quiz;
  }

  /**
   * Method to get all quizzes by user
   *
   * @return a list of quizzes, the list is a List of Quiz objects
   * @see ntnu.idatt2105.quizbackend.entity.Quiz
   * @see ntnu.idatt2105.quizbackend.repository.QuizRepository
   * @see ntnu.idatt2105.quizbackend.security.SecurityConfig
   * @see ntnu.idatt2105.quizbackend.repository.QuizRepository#findAllByCreatorId(Long)
   * @see ntnu.idatt2105.quizbackend.security.SecurityConfig#getCurrentUser()
   */
  public List<Quiz> getAllQuizzesByUser() {

    if (SecurityConfig.getCurrentUser() == null) {
      return null;
    }
    Long userId = SecurityConfig.getCurrentUser();

    return quizRepository.findAllByCreatorId(userId);
  }

  /**
   * Method for converting a Quiz object to a QuizDTO object
   *
   * @param quizRecord the quiz to be converted, the quiz is a Quiz object
   * @return the converted quiz, the converted quiz is a QuizDTO object
   * @see ntnu.idatt2105.quizbackend.dto.QuizDTO
   * @see ntnu.idatt2105.quizbackend.entity.Quiz
   * @see ntnu.idatt2105.quizbackend.entity.Question
   * @see ntnu.idatt2105.quizbackend.entity.Tag
   */
  public QuizDTO convertToQuizDTO(Quiz quizRecord) {
    QuizDTO dto = new QuizDTO();

    dto.setId(quizRecord.getId());
    dto.setName(quizRecord.getName());
    dto.setDescription(quizRecord.getDescription());
    dto.setDifficulty(quizRecord.getDifficulty());
    dto.setThumbnail_URI(quizRecord.getThumbnail());
    dto.setQuestion_ids(quizRecord.getQuestions().stream().map(Question::getId).toList());
    dto.setTag_ids(quizRecord.getTags().stream().map(Tag::getId).toList());

    return dto;
  }
}
