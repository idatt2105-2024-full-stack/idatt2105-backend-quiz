package ntnu.idatt2105.quizbackend.service;

import lombok.AllArgsConstructor;
import ntnu.idatt2105.quizbackend.dto.AttemptDTO;
import ntnu.idatt2105.quizbackend.entity.Attempt;
import ntnu.idatt2105.quizbackend.repository.AttemptRepository;
import org.springframework.stereotype.Service;


/**
 * The AttemptService class is responsible for handling the business logic for attempts.
 * It is responsible for storing attempts in the database and converting attempts to DTOs.
 *
 * @Service is used to make sure that Spring picks up this class as a service.
 * @AllArgsConstructor is used to automatically generate a constructor with all final fields as arguments.
 */
@Service
@AllArgsConstructor
public class AttemptService {

    /**
     * The AttemptRepository object is used to access the database.
     */
    private final AttemptRepository dbAttempt;

    /**
     * This method is responsible for saving all attempts to the database.
     */
    public void storeAttempt(Attempt attempt) {
        dbAttempt.save(attempt);
    }

    /**
     * This method is responsible for converting an attempt to an AttemptDTO.
     *
     * @param attempt The list of attempts that will be converted.
     * @return The list of AttemptDTOs that were converted from the list of attempts.
     */
    public AttemptDTO convertToAttemptDTO(Attempt attempt) {
        AttemptDTO dto = new AttemptDTO();

        dto.setAttempter(attempt.getAttempter().getId());
        dto.setDate(attempt.getDate().getTime());
        dto.setQuizId(attempt.getQuiz().getId());
        dto.setQuizName(attempt.getQuiz().getName());
        dto.setScore(attempt.getScore());

        return dto;
    }
}
