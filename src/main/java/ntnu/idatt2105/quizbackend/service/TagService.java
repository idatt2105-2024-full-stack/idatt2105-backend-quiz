package ntnu.idatt2105.quizbackend.service;

import lombok.AllArgsConstructor;
import ntnu.idatt2105.quizbackend.entity.Tag;
import ntnu.idatt2105.quizbackend.repository.TagRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service for handling tag related requests
 *
 * @Service annotation is used to indicate that the class provides the mechanism for handling tag related requests
 * @AllArgsConstructor annotation is used to create a constructor with all required arguments
 */
@Service
@AllArgsConstructor
public class TagService {

  /**
   * TagRepository class is used to handle the database operations for tags
   */
  private final TagRepository tagRepository;

  /**
   * Method for getting all tags belonging to a quiz
   *
   * @param quizId the id of the quiz
   * @return a list of tags belonging to the given quiz_id
   */
  public List<Tag> getQuizTags(Long quizId) {
    return tagRepository.findTagsByQuizId(quizId);
  }

  /**
   * Method for getting all tags belonging to a question
   *
   * @param questionId the id of the question
   * @return a list of tags belonging to the given question_id
   */
  public List<Tag> getQuestionTags(Long questionId) {
    return tagRepository.findTagsByQuestionId(questionId);
  }

  /**
   * Returns all available tags.
   * @return a list of all tags stored in the database.
   */
  public List<Tag> getAllTags() {
    return tagRepository.findAll();
  }
}
