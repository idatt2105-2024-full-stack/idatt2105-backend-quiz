package ntnu.idatt2105.quizbackend.security;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * This class is responsible for configuring the security of the application.
 * It is responsible for setting up the security filter chain and the authentication filter.
 * It is also responsible for setting up the authorization rules for the different endpoints in the application.
 *
 * @Configuration is used to make sure that Spring picks up this class as a configuration class.
 * @EnableWebSecurity is used to enable web security in the application.
 * @EnableMethodSecurity is used to enable method security in the application.
 * @AllArgsConstructor is used to automatically generate a constructor with all final fields as arguments.
 */
@Configuration
@EnableWebSecurity
@EnableMethodSecurity(securedEnabled = true, jsr250Enabled = true)
@AllArgsConstructor
public class SecurityConfig {

  /**
   * The authentication filter that is used to elevate the privileges of a user based on the JWT token.
   */
  private final AuthenticationFilter authFilter;

  /**
   * This method is responsible for setting up the security filter chain and the authorization rules for the different endpoints in the application.
   * It is also responsible for setting up the session management policy of the application.
   *
   * @param http The HttpSecurity object that is used to configure the security of the application.
   * @return The SecurityFilterChain that is used to filter the requests that are made to the server.
   * @throws Exception If an error occurs during the configuration of the security filter chain.
   * @Bean is used to make sure that Spring picks up this method as a bean.
   */
  @Bean
  public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
    http.csrf(AbstractHttpConfigurer::disable)
            .authorizeHttpRequests(authorizationManagerRequestMatcherRegistry ->
                    authorizationManagerRequestMatcherRegistry
                            .requestMatchers("/api/user/**").hasAnyRole("USER")
                            .requestMatchers("/api/auth/test").hasAnyRole("USER")
                            .requestMatchers("/api/auth/**").permitAll()
                            .requestMatchers("/api/quiz/create").hasAnyRole("USER")
                            .requestMatchers("/api/quiz/user/**").hasAnyRole("USER")
                            .requestMatchers("/api/quiz/**").permitAll()
                            .requestMatchers("/api/quiz/session/**").hasAnyRole("USER", "TEMPORARY")
                            .requestMatchers("/api/quiz/**").permitAll()
                            .requestMatchers("/api/tag/**").permitAll()
                            .requestMatchers("/api/question/**").permitAll()
                            .anyRequest().authenticated())
            .httpBasic(Customizer.withDefaults())
            .sessionManagement(httpSecuritySessionManagementConfigurer ->
                    httpSecuritySessionManagementConfigurer.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
            .addFilterBefore(authFilter, UsernamePasswordAuthenticationFilter.class);


    return http.build();
  }

  /**
   * This method is responsible for getting the current user from the SecurityContextHolder.
   *
   * @return The id of the current user.
   */
  public static Long getCurrentUser() {

    if (SecurityContextHolder.getContext().getAuthentication().getPrincipal().equals("anonymousUser")) {
      return null;
    }
    return (Long) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
  }

  /**
   * This method is responsible for checking if the current user is a user.
   *
   * @return True if the current user is a user, false otherwise.
   */
  public static boolean isUser() {
    return SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream()
            .anyMatch(r -> r.getAuthority().equals("ROLE_USER"));
  }
}


