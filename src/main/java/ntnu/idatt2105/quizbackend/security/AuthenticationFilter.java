package ntnu.idatt2105.quizbackend.security;

import com.auth0.jwt.interfaces.DecodedJWT;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import ntnu.idatt2105.quizbackend.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.http.HttpHeaders;

import java.io.IOException;
import java.util.Collections;

/**
 * This filter is responsible for elevating the privileges of a user based on the JWT token
 * that is sent with the request. The token is expected to be in the Authorization header.
 * The token is expected to be a Bearer token.
 *
 * @Component is used to make sure that Spring picks up this filter as a component.
 */
@Component
public class AuthenticationFilter extends OncePerRequestFilter {

    /**
     * The string that is expected to be prepended to the token in the Authorization header.
     */
    private static final String HEADER_FIELD_PREPEND = "Bearer ";

    /**
     * The utility class that is used to decode the JWT token.
     */
    private final JwtUtil jwtUtil;

    /**
     * Constructor for the AuthenticationFilter.
     *
     * @param util The utility class that is used to decode the JWT token.
     * @Autowired is used to make sure that Spring injects the JwtUtil dependency.
     */
    @Autowired
    public AuthenticationFilter(JwtUtil util) {
        this.jwtUtil = util;
    }

    /**
     * This method is called by the filter chain when a request is made to the server.
     * It is responsible for elevating the privileges of the user based on the JWT token.
     *
     * @param request The request that was made to the server.
     * @param response The response that will be sent back to the client.
     * @param filterChain The filter chain that this filter is a part of.
     * @throws ServletException If an error occurs during the request.
     * @throws IOException If an error occurs during the request.
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        // Extract the header containing the JWT
        final String header = request.getHeader(HttpHeaders.AUTHORIZATION);

        // Check that the request contained a token.
        if (header == null || !header.startsWith(HEADER_FIELD_PREPEND)) {
            filterChain.doFilter(request, response);
            return;
        }

        // Decode the token
        String encodedToken = header.substring(HEADER_FIELD_PREPEND.length());
        DecodedJWT token = jwtUtil.decodeToken(encodedToken);

        // Skip elevation if the token could not be decoded, or it has expired.
        if (token == null || jwtUtil.isTokenExpired(token)) {
            filterChain.doFilter(request, response);
            return;
        }

        // The user seems to deserve elevation!

        // Extract subject
        final String subject = token.getSubject();
        final String[] parts = subject.split(":");

        // Extract the user
        final String role = parts[0];
        final Long userId = Long.valueOf(parts[1]);

        // Upgrade session privileges
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(
                userId,
                null,
                Collections.singletonList(new SimpleGrantedAuthority(role))));

        filterChain.doFilter(request, response);
    }
}