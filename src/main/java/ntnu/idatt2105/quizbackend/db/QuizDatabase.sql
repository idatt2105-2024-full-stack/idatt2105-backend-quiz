SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS User;
DROP TABLE IF EXISTS Quiz;
DROP TABLE IF EXISTS Question;
DROP TABLE IF EXISTS Answer;
DROP TABLE IF EXISTS Keyword;
DROP TABLE IF EXISTS SharedQuestion;
DROP TABLE IF EXISTS QuestionAnswer;
DROP TABLE IF EXISTS QuizKeywords;
DROP TABLE IF EXISTS PastQuiz;
SET FOREIGN_KEY_CHECKS = 1;

CREATE TABLE User
(
    user_id  INT AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    email    VARCHAR(255) NOT NULL
);

CREATE TABLE Quiz
(
    quiz_id    INT AUTO_INCREMENT PRIMARY KEY,
    user_id    INT          NOT NULL,
    name       VARCHAR(255) NOT NULL,
    difficulty INT          NOT NULL,
    category   VARCHAR(255) NOT NULL,
    image_url  VARCHAR(255),
    FOREIGN KEY (user_id) REFERENCES user (user_id)
);

CREATE TABLE Question
(
    question_id   INT AUTO_INCREMENT PRIMARY KEY,
    user_id       INT          NOT NULL,
    question_text VARCHAR(255) NOT NULL,
    FOREIGN KEY (user_id) REFERENCES user (user_id)
);

CREATE TABLE Answer
(
    answer_id   INT AUTO_INCREMENT PRIMARY KEY,
    answer_text VARCHAR(255) NOT NULL,
    is_correct  BOOLEAN      NOT NULL
);

CREATE TABLE Keyword
(
    keyword_id INT AUTO_INCREMENT PRIMARY KEY,
    keyword    VARCHAR(255) NOT NULL
);

CREATE TABLE SharedQuestion
(
    quiz_id     INT NOT NULL,
    question_id       INT NOT NULL,
    FOREIGN KEY (quiz_id) REFERENCES quiz (quiz_id),
    FOREIGN KEY (question_id) REFERENCES question (question_id),
    PRIMARY KEY (quiz_id, question_id)
);

CREATE TABLE QuestionAnswer
(
    question_id       INT NOT NULL,
    answer_id         INT NOT NULL,
    FOREIGN KEY (question_id) REFERENCES question (question_id),
    FOREIGN KEY (answer_id) REFERENCES answer (answer_id),
    PRIMARY KEY (question_id, answer_id)
);

CREATE TABLE QuizKeywords
(
    quiz_id         INT NOT NULL,
    keyword_id      INT NOT NULL,
    FOREIGN KEY (quiz_id) REFERENCES quiz (quiz_id),
    FOREIGN KEY (keyword_id) REFERENCES keyword (keyword_id),
    PRIMARY KEY (quiz_id, keyword_id)
);

CREATE TABLE PastQuiz
(
    pastQuiz_id INT AUTO_INCREMENT PRIMARY KEY,
    quiz_id     INT NOT NULL,
    user_id     INT NOT NULL,
    question_id INT NOT NULL,
    answer_id   INT NOT NULL,
    FOREIGN KEY (user_id) REFERENCES user (user_id),
    FOREIGN KEY (quiz_id) REFERENCES quiz (quiz_id),
    FOREIGN KEY (question_id) REFERENCES question (question_id),
    FOREIGN KEY (answer_id) REFERENCES answer (answer_id)
);

ALTER TABLE User ENGINE = InnoDB;
ALTER TABLE Quiz ENGINE = InnoDB;
ALTER TABLE Question ENGINE = InnoDB;
ALTER TABLE Answer ENGINE = InnoDB;
ALTER TABLE Keyword ENGINE = InnoDB;
ALTER TABLE SharedQuestion ENGINE = InnoDB;
ALTER TABLE QuestionAnswer ENGINE = InnoDB;
ALTER TABLE QuizKeywords ENGINE = InnoDB;
ALTER TABLE PastQuiz ENGINE = InnoDB;
