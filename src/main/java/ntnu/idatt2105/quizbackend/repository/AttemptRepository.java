package ntnu.idatt2105.quizbackend.repository;

import ntnu.idatt2105.quizbackend.entity.Attempt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Repository for the Attempt entity
 *
 * @Repository annotation is used to indicate that the class provides the mechanism for storage, retrieval, search, update and delete operation on objects.
 * @see ntnu.idatt2105.quizbackend.entity.Attempt
 */
@Repository
public interface AttemptRepository extends JpaRepository<Attempt, Long> {}
