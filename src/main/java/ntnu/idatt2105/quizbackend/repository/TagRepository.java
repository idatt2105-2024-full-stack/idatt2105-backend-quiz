package ntnu.idatt2105.quizbackend.repository;

import ntnu.idatt2105.quizbackend.entity.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository for the Tag entity
 *
 * @Repository annotation is used to indicate that the class provides the mechanism for storage, retrieval, search, update and delete operation on objects.
 * @see ntnu.idatt2105.quizbackend.entity.Tag
 */
@Repository
public interface TagRepository extends JpaRepository<Tag, Long> {

  /**
   * Method to find tags by quiz id
   *
   * @param quizId the id of the quiz, the id is a Long
   * @return a list of tags that belong to the quiz with the given id
   */
  @Query(value = "SELECT t.tag_id, t.tag FROM tag t JOIN quiz_tags qt ON t.tag_id = qt.tag_id JOIN quiz q ON qt.quiz_id = q.quiz_id WHERE q.quiz_id = :quizId", nativeQuery = true)
  List<Tag> findTagsByQuizId(@Param("quizId") Long quizId);

  /**
   * Method to find tags by question id
   *
   * @param questionId the id of the question, the id is a Long
   * @return a list of tags that belong to the question with the given id
   */
  @Query(value = "SELECT t.tag_id, t.tag FROM tag t JOIN question_tags qt ON t.tag_id = qt.tag_id JOIN question q ON qt.question_id = q.question_id WHERE q.question_id = :questionId", nativeQuery = true)
  List<Tag> findTagsByQuestionId(@Param("questionId") Long questionId);
}
