package ntnu.idatt2105.quizbackend.repository;

import ntnu.idatt2105.quizbackend.entity.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository for the Question entity
 *
 * @Repository annotation is used to indicate that the class provides the mechanism for storage, retrieval, search, update and delete operation on objects.
 * @see ntnu.idatt2105.quizbackend.entity.Question
 */
@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {

  /**
   * Method to find a question by its id
   *
   * @param id the id of the question, the id is a Long
   * @return the question with the given id
   */
  Question findQuestionById(Long id);

  /**
   * Method to find a question by its creator id
   *
   * @param userId the id of the creator, the id is a Long
   * @return a list of questions with the given creator id
   */
  List<Question> findQuestionsByCreatorId(Long userId);
}
