package ntnu.idatt2105.quizbackend.repository;

import ntnu.idatt2105.quizbackend.entity.Quiz;
import ntnu.idatt2105.quizbackend.entity.Tag;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Repository for the Quiz entity
 *
 * @Repository annotation is used to indicate that the class provides the mechanism for storage, retrieval, search, update and delete operation on objects.
 * @see ntnu.idatt2105.quizbackend.entity.Quiz
 */
@Repository
public interface QuizRepository extends JpaRepository<Quiz, Long> {

  /**
   * Method to find a quiz by its id
   *
   * @param id the id of the quiz, the id is a Long
   * @return the quiz with the given id
   */
  Quiz findQuizById(Long id);

  /**
   * Method to find all quizzes by creator id
   *
   * @param userId the id of the creator, the id is a Long
   * @return a list of quizzes that belong to the creator with the given id
   */
  List<Quiz> findAllByCreatorId(Long userId);

  /**
   * Method to find all quizzes by creator id in a pageable format
   *
   * @param userId the id of the creator, the id is a Long
   * @param pageable the pageable object
   * @return a list of quizzes that belong to the creator with the given id
   */
  List<Quiz> findAllByCreatorId(Long userId, Pageable pageable);

  /**
   * Method to find all quizzes by tags in a pageable format
   *
   * @param tag the list of tags, the tags are a List of Tag
   * @return a list of quizzes that belong to the tags with the given tags
   */
  List<Quiz> findAllByTags(List<Tag> tag, Pageable pageable);

  /**
   * Method for getting six random quizzes from the database in a pageable format
   *
   * @return a list of six random quizzes
   */
  @Query(value = "SELECT * FROM quiz ORDER BY RAND() LIMIT 6", nativeQuery = true)
  List<Quiz> findSixRandomQuizzes();

  /**
   * Method to find all quizzes by creator id and tags in a pageable format
   *
   * @param userId the id of the creator, the id is a Long
   * @param tags the list of tags, the tags are a List of Tag
   * @param pageable the pageable object
   * @return a list of quizzes that belong to the creator with the given id and the tags with the given tags
   */
  List<Quiz> findAllByCreatorIdAndTags(Long userId, List<Tag> tags, Pageable pageable);
}
