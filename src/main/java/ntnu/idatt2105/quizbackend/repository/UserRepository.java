package ntnu.idatt2105.quizbackend.repository;

import ntnu.idatt2105.quizbackend.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Repository for the User entity
 *
 * @Repository The Lombok annotation Repository is used to indicate that the class is a repository. The repository is used to access the database.
 * @see ntnu.idatt2105.quizbackend.entity.User
 */
@Repository
public interface UserRepository extends JpaRepository<User, String> {

  /**
   * Method to find a user by its email
   *
   * @param email the email of the user, the email is a String
   * @return the user with the given email
   */
  User findUserByEmail(String email);

  /**
   * Method to find a user by its id
   *
   * @param id the id of the user, the id is a Long
   * @return the user with the given id
   */
  User findUserById(Long id);

  /**
   * Method to find a users password by its email
   *
   * @param email the email of the user, the email is a String
   * @return the password of the user with the given email
   */
  @Query(value = "SELECT password FROM users u WHERE u.email = :email", nativeQuery = true)
  String findPasswordByEmail(@Param("email") String email);

  /**
   * Method to find a users id by its email
   *
   * @param email the email of the user, the email is a String
   * @return the id of the user with the given email
   */
  @Query(value = "SELECT id FROM users u WHERE u.email = :email", nativeQuery = true)
  Long findUserIdByEmail(@Param("email") String email);

  /**
   * Method to check if a user exists by its email
   *
   * @param email the email of the user, the email is a String
   * @return a boolean value indicating if the user exists
   */
  boolean existsByEmail(String email);
}