package ntnu.idatt2105.quizbackend;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * The main class of the application
 *
 * @SpringBootApplication is a convenience annotation that adds the required annotations for the application to run
 */
@SpringBootApplication
public class QuizBackendApplication {

	/**
	 * The main method of the application
	 *
	 * @param args the arguments passed to the application
	 */
	public static void main(String[] args) {
		SpringApplication.run(QuizBackendApplication.class, args);
	}

	/**
	 * Method to create a ModelMapper bean
	 *
	 * @return a ModelMapper object
	 */
	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}
}
