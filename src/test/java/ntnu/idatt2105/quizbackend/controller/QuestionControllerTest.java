package ntnu.idatt2105.quizbackend.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.transaction.Transactional;
import ntnu.idatt2105.quizbackend.dto.AnswerDTO;
import ntnu.idatt2105.quizbackend.dto.QuestionDTO;
import ntnu.idatt2105.quizbackend.entity.Question;
import ntnu.idatt2105.quizbackend.entity.Tag;
import ntnu.idatt2105.quizbackend.entity.User;
import ntnu.idatt2105.quizbackend.repository.QuestionRepository;
import ntnu.idatt2105.quizbackend.repository.TagRepository;
import ntnu.idatt2105.quizbackend.repository.UserRepository;
import ntnu.idatt2105.quizbackend.security.SecurityConfig;
import ntnu.idatt2105.quizbackend.service.QuestionService;
import ntnu.idatt2105.quizbackend.util.JwtUtil;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(QuestionController.class)
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
@AutoConfigureDataJpa
@Import({ SecurityConfig.class, QuestionController.class, QuestionService.class, JwtUtil.class })
@Transactional
public class QuestionControllerTest {

    private static final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private MockMvc mvc;

    @Autowired
    private UserRepository dbUser;

    @Autowired
    private QuestionRepository dbQuestion;

    @Autowired
    private TagRepository dbTag;

    @Autowired
    private JwtUtil jwtUtil;

    private static final QuestionDTO testQuestion1 = new QuestionDTO();
    private static final QuestionDTO testQuestion2 = new QuestionDTO();

    @Test
    public void createNewQuestions_storesDataCorrectlyInDatabase() throws Exception {
        // Need a temporary user in the database to create the question
        User u = putTestUserInDB();

        // Add some temporary tags in the database
        List<Tag> tags = putTagsInDatabase();

        // Need a temporary token to send the query
        String JWT = jwtUtil.createUserToken(u.getId());

        mvc.perform(post("/api/user/question")
                        .content(mapper.writeValueAsString(List.of(testQuestion1, testQuestion2)))
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer " + JWT))
                .andExpect(status().isCreated());

        List<Question> savedQuestions = dbQuestion.findAll();
        Question savedQuestion1 = savedQuestions.get(0);
        Question savedQuestion2 = savedQuestions.get(1);

        assertEquals(savedQuestion1.getText(), testQuestion1.getQuestion());
        assertEquals(savedQuestion1.getResource_url(), testQuestion1.getContentURI());
        assertEquals(savedQuestion1.getTags().stream().map(Tag::getId).toList(), testQuestion1.getTag_ids());
        assertEquals(savedQuestion1.getCreator(), u);
        assertEquals(savedQuestion1.getAnswers().stream().map(QuestionService::convertToAnswerDTO).toList(), testQuestion1.getPossibleAnswers());

        assertEquals(savedQuestion2.getText(), testQuestion2.getQuestion());
        assertEquals(savedQuestion2.getResource_url(), testQuestion2.getContentURI());
        assertEquals(savedQuestion2.getTags().stream().map(Tag::getId).toList(), testQuestion2.getTag_ids());
        assertEquals(savedQuestion2.getCreator(), u);
        assertEquals(savedQuestion2.getAnswers().stream().map(QuestionService::convertToAnswerDTO).toList(), testQuestion2.getPossibleAnswers());
    }

    private User putTestUserInDB() {
        User newUser1 = new User();

        newUser1.setUsername("FireLights99");
        newUser1.setEmail("hfredrif@gmail.com");
        newUser1.setPassword("3498dj02349urwfpe9");

        dbUser.save(newUser1);

        return newUser1;
    }

    private List<Tag> putTagsInDatabase() {
        return dbTag.saveAll(List.of(
                new Tag("Tag: Q1"),
                new Tag("Tag: Q2")
        ));
    }

    @BeforeAll
    private static void setup() {
        testQuestion1.setQuestion("Test-Question 1");
        testQuestion1.setContentURI("URI 1");
        testQuestion1.setTag_ids(List.of(1L));
        testQuestion1.setPossibleAnswers(List.of(
                new AnswerDTO("Wrong answer 1", false),
                new AnswerDTO("Correct answer 1", true),
                new AnswerDTO("Decoy answer 1", false)
                ));

        testQuestion2.setQuestion("Test-Question 2");
        testQuestion2.setContentURI("URI 2");
        testQuestion2.setTag_ids(List.of(2L));
        testQuestion2.setPossibleAnswers(List.of(
                new AnswerDTO("Wrong answer 2", false),
                new AnswerDTO("Correct answer 2", true),
                new AnswerDTO("Decoy answer 2", false)
        ));
    }
}
