package ntnu.idatt2105.quizbackend.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.transaction.Transactional;
import ntnu.idatt2105.quizbackend.dto.PageableDTO;
import ntnu.idatt2105.quizbackend.dto.QuizDTO;
import ntnu.idatt2105.quizbackend.entity.*;
import ntnu.idatt2105.quizbackend.repository.*;
import ntnu.idatt2105.quizbackend.security.SecurityConfig;
import ntnu.idatt2105.quizbackend.service.AuthenticationService;
import ntnu.idatt2105.quizbackend.service.QuestionService;
import ntnu.idatt2105.quizbackend.service.QuizService;
import ntnu.idatt2105.quizbackend.service.TagService;
import ntnu.idatt2105.quizbackend.util.JwtUtil;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(QuizController.class)
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
@AutoConfigureDataJpa
@Import({QuizService.class, SecurityConfig.class, JwtUtil.class, AuthenticationService.class, QuestionController.class, TagController.class, QuestionService.class, TagService.class})
public class QuizControllerTest {

  @Autowired
  private MockMvc mvc;

  @Autowired
  private JwtUtil jwtUtil;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private QuizRepository quizRepository;

  @Autowired
  private QuestionRepository questionRepository;

  @Autowired
  private AnswerRepository answerRepository;

  @Autowired
  private TagRepository tagRepository;

  @Autowired
  private static QuizRepository quizRepositoryFinal;

  private static final ObjectMapper mapper = new ObjectMapper();
  private static final ModelMapper modelMapper = new ModelMapper();

  private static final User creator = new User();

  private static final Question question1 = new Question();
  private static final Question question2 = new Question();

  private static final Answer answer1 = new Answer();
  private static final Answer answer2 = new Answer();
  private static final Answer answer3 = new Answer();
  private static final Answer answer4 = new Answer();


  private static final Quiz quiz1 = new Quiz();
  private static final Quiz quiz2 = new Quiz();
  private static final Quiz quiz3 = new Quiz();
  private static final Quiz quiz4 = new Quiz();

  private static final Tag tag1 = new Tag();
  private static final Tag tag2 = new Tag();
  private static final Tag tag3 = new Tag();
  private static final Tag tag4 = new Tag();

  @BeforeAll
  public static void setUp() {

    creator.setId(1L);
    creator.setUsername("testuser");
    creator.setEmail("test");
    creator.setPassword("password");

    question1.setId(2L);
    question1.setText("Test question 1");

    question2.setId(3L);
    question2.setText("Test question 2");

    answer1.setId(1L);
    answer1.setText("Test answer 1");
    answer1.setCorrectAnswer(true);
    answer1.setQuestion(question1);

    answer2.setId(2L);
    answer2.setText("Test answer 2");
    answer2.setCorrectAnswer(false);
    answer2.setQuestion(question1);

    answer3.setId(3L);
    answer3.setText("Test answer 1");
    answer3.setCorrectAnswer(true);
    answer3.setQuestion(question2);

    answer4.setId(4L);
    answer4.setText("Test answer 2");
    answer4.setCorrectAnswer(false);
    answer4.setQuestion(question2);

    question1.setCreator(creator);
    question2.setCreator(creator);

    question1.setAnswers(List.of(answer1, answer2));
    question2.setAnswers(List.of(answer3, answer4));

    quiz1.setId(1L);
    quiz1.setName("Quiz 1");
    quiz1.setDescription("Description of Quiz 1");
    quiz1.setCreator(creator);

    quiz2.setId(2L);
    quiz2.setName("Quiz 2");
    quiz2.setDescription("Description of Quiz 2");
    quiz2.setCreator(creator);

    quiz3.setId(3L);
    quiz3.setName("Quiz 3");
    quiz3.setDescription("Description of Quiz 3");
    quiz3.setCreator(creator);


    quiz4.setId(4L);
    quiz4.setName("Quiz 4");
    quiz4.setDescription("Description of Quiz 4");
    quiz4.setCreator(creator);

    tag1.setId(1L);
    tag1.setTag("Tag 1");

    tag2.setId(2L);
    tag2.setTag("Tag 2");

    tag3.setId(3L);
    tag3.setTag("Tag 3");

    tag4.setId(4L);
    tag4.setTag("Tag 4");
  }

  @BeforeEach
  public void cleanDatabase() {
    tagRepository.deleteAll();
    answerRepository.deleteAll();
    quizRepository.deleteAll();
    questionRepository.deleteAll();

    userRepository.deleteAll();
  }

  @Test
  @Transactional
  public void retrieveQuizPaginated() throws Exception {
    // Mocking input parameters
    int page = 0;
    int size2 = 2;
    int size4 = 4;

    User userDB = userRepository.save(creator);

    question1.setCreator(userDB);
    question2.setCreator(userDB);

    List<Question> questionDB = questionRepository.saveAll(List.of(question1, question2));

    //List<Tag> tagsDB = tagRepository.saveAll(List.of(tag1, tag2, tag3, tag4));

    quiz1.setQuestions(questionDB);
    //quiz1.setTags(tagsDB);
    quiz1.setCreator(userDB);

    quiz2.setQuestions(questionDB);
    //quiz2.setTags(tagsDB);
    quiz2.setCreator(userDB);

    quiz3.setQuestions(questionDB);
    //quiz3.setTags(tagsDB);
    quiz3.setCreator(userDB);

    quiz4.setQuestions(questionDB);
    //quiz4.setTags(tagsDB);
    quiz4.setCreator(userDB);



    List<Quiz> quizzesTest = quizRepository.saveAll(List.of(quiz1, quiz2, quiz3, quiz4));

    List<QuizDTO> quizDTOsTest = quizzesTest.stream()
            .map(quiz -> modelMapper.map(quiz, QuizDTO.class))
            .toList();


    PageableDTO pageableDTO2 = new PageableDTO();
    pageableDTO2.setPage(page);
    pageableDTO2.setSize(size2);

    PageableDTO pageableDTO4 = new PageableDTO();
    pageableDTO4.setPage(page);
    pageableDTO4.setSize(size4);

    String JWT = jwtUtil.createUserToken(creator.getId());
    System.out.println(JWT);

    String quizzes = mvc.perform(post("/api/quiz/get/paginated")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(mapper.writeValueAsBytes(pageableDTO2)))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();

    List<QuizDTO> quizDTOs = mapper.readerForListOf(QuizDTO.class).readValue(quizzes);

    for (Object quizDTO : quizDTOs) {
      System.out.println(quizDTO.toString());
    }

    assertEquals(2, quizDTOs.size());
    for (int i = 0; i < quizDTOs.size(); i++) {
      assertEquals(quizDTOsTest.get(i).getId(), quizDTOs.get(i).getId());
      assertEquals(quizDTOsTest.get(i).getName(), quizDTOs.get(i).getName());
    }

    quizzes = mvc.perform(post("/api/quiz/get/paginated")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(mapper.writeValueAsBytes(pageableDTO4)))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();

    quizDTOs = mapper.readerForListOf(QuizDTO.class).readValue(quizzes);

    for (Object quizDTO : quizDTOs) {
      System.out.println(quizDTO.toString());
    }

    assertEquals(4, quizDTOs.size());
    for (int i = 0; i < quizDTOs.size(); i++) {
      assertEquals(quizDTOsTest.get(i).getId(), quizDTOs.get(i).getId());
      assertEquals(quizDTOsTest.get(i).getName(), quizDTOs.get(i).getName());
    }
  }

  @Test
  public void createQuiz() throws Exception {

    //creator.setId(2L);

    User userDB = userRepository.save(creator);

    tagRepository.saveAll(List.of(tag1, tag2, tag3, tag4));

    List<Long> tagIds = List.of(tag1.getId(), tag2.getId(), tag3.getId(), tag4.getId());

    question1.setAnswers(new ArrayList<>());
    question2.setAnswers(new ArrayList<>());
    question1.setId(3L);
    question2.setId(4L);
    question1.setCreator(userDB);
    question2.setCreator(userDB);

    Question questionDB1 = questionRepository.save(question1);
    Question questionDB2 = questionRepository.save(question2);

    List<Long> questionIds = List.of(questionDB1.getId(), questionDB2.getId());

    QuizDTO quizDTO = new QuizDTO();

    quizDTO.setId(1L);
    quizDTO.setName("Test Name");
    quizDTO.setDescription("Test Description");
    quizDTO.setDifficulty(3);
    quizDTO.setTag_ids(tagIds);
    quizDTO.setQuestion_ids(questionIds);
    quizDTO.setCreator_id(userDB.getId());

    String JWT = jwtUtil.createUserToken(userDB.getId());

    String response = mvc.perform(post("/api/quiz/create")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(mapper.writeValueAsString(quizDTO))
                    .header("Authorization", "Bearer " + JWT))
            .andExpect(status().isCreated())
            .andReturn()
            .getResponse()
            .getContentAsString();

    QuizDTO quizDTODB = mapper.readValue(response, QuizDTO.class);

    Quiz quiz = quizRepository.findById(quizDTODB.getId()).orElse(null);

    assertEquals(quizDTODB.getId(), quiz.getId());
    assertEquals(quizDTODB.getName(), quiz.getName());
  }

//  @Test
//  public void retrieveQuizPaginatedByTags() throws Exception {
//
//    creator.setId(3L);
//
//    userRepository.save(creator);
//
//    tagRepository.saveAll(List.of(tag1, tag2, tag3, tag4));
//
//    question1.setAnswers(new ArrayList<>());
//    question2.setAnswers(new ArrayList<>());
//    question1.setId(5L);
//    question1.setCreator(creator);
//    question2.setId(6L);
//    question2.setCreator(creator);
//
//    questionRepository.save(question1);
//    questionRepository.save(question2);
//
//    answer1.setQuestion(question1);
//    answer1.setId(7L);
//
//    answer2.setQuestion(question1);
//    answer2.setId(8L);
//
//    answer3.setQuestion(question2);
//    answer3.setId(9L);
//
//    answer4.setQuestion(question2);
//    answer4.setId(10L);
//
//    answerRepository.saveAll(List.of(answer1, answer2, answer3, answer4));
//
//    question1.setAnswers(List.of(answer1, answer2));
//    question2.setAnswers(List.of(answer3, answer4));
//    questionRepository.saveAll(List.of(question1, question2));
//
//    quiz1.setQuestions(List.of(question1, question2));
//    quiz1.setTags(List.of(tag1, tag2));
//    quiz1.setId(5L);
//    List<Tag> tags12 = List.of(tag1, tag2);
//
//    quiz2.setQuestions(List.of(question1, question2));
//    quiz2.setTags(List.of(tag2, tag3));
//    quiz2.setId(6L);
//    List<Tag> tags23 = List.of(tag2, tag3);
//
//    quiz3.setQuestions(List.of(question1, question2));
//    quiz3.setTags(List.of(tag3, tag4));
//    quiz3.setId(7L);
//    List<Tag> tags34 = List.of(tag3, tag4);
//
//    quiz4.setQuestions(List.of(question1, question2));
//    quiz4.setTags(List.of(tag4, tag1));
//    quiz4.setId(8L);
//    List<Tag> tags41 = List.of(tag4, tag1);
//
//    quizRepository.saveAll(List.of(quiz1, quiz2, quiz3, quiz4));
//
//    List<Quiz> quizzesTest = List.of(quiz1, quiz2, quiz3, quiz4);
//
//    List<QuizDTO> quizDTOsTest = quizzesTest.stream()
//            .map(quiz -> modelMapper.map(quiz, QuizDTO.class))
//            .toList();
//
//    PageableDTO pageableDTO = new PageableDTO();
//    pageableDTO.setPage(0);
//    pageableDTO.setSize(4);
//
//    String JWT = jwtUtil.createUserToken(creator.getId());
//
//    String quizzes = mvc.perform(post("/api/quiz/get/paginated/tags")
//                    .contentType(MediaType.APPLICATION_JSON)
//                    .content(mapper.writeValueAsString(pageableDTO))
//                    .content(mapper.writeValueAsBytes(tags12))
//                    .header("Authorization", "Bearer " + JWT))
//            .andExpect(status().isOk())
//            .andReturn()
//            .getResponse()
//            .getContentAsString();
//
//    List<QuizDTO> quizDTOs = mapper.readerForListOf(QuizDTO.class).readValue(quizzes);
//
//    for (Object quizDTO : quizDTOs) {
//      System.out.println(quizDTO.toString());
//    }
//
//    assertEquals(2, quizDTOs.size());
//    assertEquals(quizDTOsTest.get(0).getId(), quizDTOs.get(0).getId());
//    assertEquals(quizDTOsTest.get(0).getName(), quizDTOs.get(0).getName());
//  }
}
