package ntnu.idatt2105.quizbackend.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.transaction.Transactional;
import ntnu.idatt2105.quizbackend.dto.AuthenticateUserDTO;
import ntnu.idatt2105.quizbackend.dto.NewUserDTO;
import ntnu.idatt2105.quizbackend.dto.TokenAnswerDTO;
import ntnu.idatt2105.quizbackend.entity.User;
import ntnu.idatt2105.quizbackend.repository.UserRepository;
import ntnu.idatt2105.quizbackend.security.SecretsConfig;
import ntnu.idatt2105.quizbackend.security.SecurityConfig;
import ntnu.idatt2105.quizbackend.service.AuthenticationService;
import ntnu.idatt2105.quizbackend.util.JwtUtil;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;



@WebMvcTest(AuthenticationController.class)
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
@AutoConfigureDataJpa
@Import({ SecurityConfig.class, AuthenticationController.class, JwtUtil.class, AuthenticationService.class})
@Transactional
public class AuthenticationControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private UserRepository database;

    @Autowired
    private SecretsConfig secrets;

    private static final ObjectMapper mapper = new ObjectMapper();

    private static final User mockUser1 = new User();
    private static final User mockUser2 = new User();
    private static final User newUser1 = new User();

    @BeforeAll
    public static void setup() {
        mockUser1.setUsername("Hans-Tore");
        mockUser1.setEmail("hato@gmail.com");
        mockUser1.setPassword("Passopp123");

        mockUser2.setUsername("Supermann56");
        mockUser2.setEmail("tcmalvik@gmail.com");
        mockUser2.setPassword("09283rowei");

        newUser1.setUsername("FireLights99");
        newUser1.setEmail("hfredrif@gmail.com");
        newUser1.setPassword("3498dj02349urwfpe9");
    }

    @BeforeEach
    public void cleanDatabase() {
        database.deleteAll();
    }

    @Test
    public void registerNewUser_createsCorrectUserInDatabase() throws Exception {
        NewUserDTO dto = new NewUserDTO(newUser1.getEmail(), newUser1.getUsername(), newUser1.getPassword());

        String response = mvc.perform(post("/api/auth/register")
                        .content(mapper.writeValueAsString(dto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString();

        assertNotNull(response);

        User newUserEntry = database.findUserByEmail(newUser1.getEmail());
        assertEquals(newUserEntry.getEmail(), newUser1.getEmail());
        assertEquals(newUserEntry.getUsername(), newUser1.getUsername());
        assertEquals(newUserEntry.getPassword(), newUser1.getPassword());
    }

    @Test
    public void registerNewUser_returns409_andFalseSuccess_whenUserExists() throws Exception {
        database.save(newUser1);
        NewUserDTO dto = new NewUserDTO(newUser1.getEmail(), newUser1.getUsername(), newUser1.getPassword());

        String response = mvc.perform(post("/api/auth/register")
                        .content(mapper.writeValueAsString(dto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict())
                .andReturn()
                .getResponse()
                .getContentAsString();

        assertNotNull(response);

        TokenAnswerDTO answer = mapper.readValue(response, TokenAnswerDTO.class);

        assertFalse(answer.isSuccess());
        assertNull(answer.getToken());
    }

    @Test
    public void loginReturns401_whenGivenWrongCredentials() throws Exception {
        database.save(newUser1);

        AuthenticateUserDTO dto = new AuthenticateUserDTO(newUser1.getEmail(), "wrong_password");

        mvc.perform(post("/api/auth")
                        .content(mapper.writeValueAsString(dto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void loginReturns401_whenUserDoesNotExist() throws Exception {
        database.save(newUser1);

        AuthenticateUserDTO dto = new AuthenticateUserDTO("nonexistant@mail.com", "wrong_password");

        mvc.perform(post("/api/auth")
                        .content(mapper.writeValueAsString(dto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void loginSuccessful_withCorrectCredentials() throws Exception {
        database.save(newUser1);

        AuthenticateUserDTO dto = new AuthenticateUserDTO(newUser1.getEmail(), newUser1.getPassword());

        mvc.perform(post("/api/auth")
                        .content(mapper.writeValueAsString(dto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void loginSuccessful_andBodyIsReturnedCorrectly() throws Exception {
        database.save(newUser1);

        AuthenticateUserDTO dto = new AuthenticateUserDTO(newUser1.getEmail(), newUser1.getPassword());

        String responseBody = mvc.perform(post("/api/auth")
                        .content(mapper.writeValueAsString(dto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        TokenAnswerDTO answer = mapper.readValue(responseBody, TokenAnswerDTO.class);
        assertTrue(answer.isSuccess());
        assertNotNull(answer.getToken());
    }

    @Test
    public void saltEndpointReturnsCorrectly() throws Exception {
        String responseBody = mvc.perform(get("/api/auth"))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        assertEquals(secrets.getSalt(), responseBody);
    }

    @Test
    public void testingEndpointWorks() throws Exception {
        database.save(newUser1);
        NewUserDTO dto = new NewUserDTO(newUser1.getEmail(), newUser1.getUsername(), newUser1.getPassword());

        String response = mvc.perform(post("/api/auth")
                        .content(mapper.writeValueAsString(dto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        System.out.println("This is the response after registering" + response);

        assertNotNull(response);

        TokenAnswerDTO answer = mapper.readValue(response, TokenAnswerDTO.class);

        mvc.perform(get("/api/auth/test")
                        .header("Authorization", "Bearer " + answer.getToken()))
                .andExpect(status().isOk());
    }

    @Test
    public void registeringValidEmail_returns201() throws Exception {
        String[] valid_mails = {
                "mail@mail.com",
                "mail.mail@mail.com",
                "super.long.mail@in.no"
        };

        NewUserDTO dto = new NewUserDTO("", "username", "dj2qdojw34j");

        for (String email : valid_mails) {
            dto.setEmail(email);

            mvc.perform(post("/api/auth/register")
                            .content(mapper.writeValueAsString(dto))
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isCreated());
        }
    }

    @Test
    public void registeringInvalidEmail_returns400() throws Exception {
        String[] invalid_mails = {
                "mail.mail.com",
                "mail@com",
                "@mail.com",
                "mail@.com",
                "mail@com",
                "mail@mail.com.",
                ".mail@mail.com",
                "mail@mail@mail.com",
                "super.long.mail@that.doesnotexist"
        };
        NewUserDTO dto = new NewUserDTO("", "username", "dj2qdojw34j");
        for (String email : invalid_mails) {
            dto.setEmail(email);
            mvc.perform(post("/api/auth/register")
                            .content(mapper.writeValueAsString(dto))
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isBadRequest());
        }
    }

    @Test
    public void loginInvalidEmail_returns400() throws Exception {
        String[] invalid_mails = {
                "mail.mail.com",
                "mail@com",
                "@mail.com",
                "mail@.com",
                "mail@com",
                "mail@mail.com.",
                ".mail@mail.com",
                "mail@mail@mail.com",
                "super.long.mail@that.doesnotexist"
        };

        AuthenticateUserDTO dto = new AuthenticateUserDTO("", "dj2qdojw34j");
        for (String email : invalid_mails) {
            dto.setEmail(email);
            mvc.perform(post("/api/auth")
                            .content(mapper.writeValueAsString(dto))
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isBadRequest());
        }
    }
}
