package ntnu.idatt2105.quizbackend.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.transaction.Transactional;
import ntnu.idatt2105.quizbackend.dto.TagCollectionDTO;
import ntnu.idatt2105.quizbackend.dto.TagDTO;
import ntnu.idatt2105.quizbackend.entity.Question;
import ntnu.idatt2105.quizbackend.entity.Quiz;
import ntnu.idatt2105.quizbackend.entity.User;
import ntnu.idatt2105.quizbackend.repository.QuestionRepository;
import ntnu.idatt2105.quizbackend.repository.QuizRepository;
import ntnu.idatt2105.quizbackend.repository.TagRepository;
import ntnu.idatt2105.quizbackend.repository.UserRepository;
import ntnu.idatt2105.quizbackend.security.SecurityConfig;
import ntnu.idatt2105.quizbackend.service.TagService;
import ntnu.idatt2105.quizbackend.util.JwtUtil;
import ntnu.idatt2105.quizbackend.entity.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(TagController.class)
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
@AutoConfigureDataJpa
@Import({SecurityConfig.class, TagController.class, TagService.class, JwtUtil.class})
@Transactional
public class TagControllerTest {

  @Autowired
  private TagRepository dbTag;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private QuizRepository quizRepository;

  @Autowired
  private QuestionRepository questionRepository;

  @Autowired
  private MockMvc mvc;

  private static final ObjectMapper mapper = new ObjectMapper();

  @Test
  public void getAllTags_returnsAllTags() throws Exception {
    Tag[] tags = {
            new Tag("Tag 1"),
            new Tag("Tag 2"),
            new Tag("Tag 3")
    };

    dbTag.deleteAll();

    dbTag.saveAll(Arrays.asList(tags));

    String response = mvc.perform(get("/api/tag"))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();

    List<TagDTO> returnedDTOs = mapper.readerForListOf(TagDTO.class).readValue(response);

    System.out.println(returnedDTOs.size());
    System.out.println(tags.length);

    System.out.println(returnedDTOs.toString());
    System.out.println(Arrays.toString(tags));

    for (int i = 0; i < tags.length; i++) {
      assertEquals(tags[i].getId(), returnedDTOs.get(i).getId());
      assertEquals(tags[i].getTag(), returnedDTOs.get(i).getTag());
    }
  }

  @Test
  public void getQuizTags_returnsAllTagsForQuiz() throws Exception {

    List<Tag> tags = Arrays.asList(
            new Tag("Tag 1"),
            new Tag("Tag 2"),
            new Tag("Tag 3")
    );

    dbTag.saveAll(tags);

    User user = new User();
    user.setId(1L);
    user.setUsername("FireLights99");
    user.setPassword("password");
    user.setEmail("fire@gmail.com");

    User userDB = userRepository.save(user);

    Quiz quiz = new Quiz();
    quiz.setId(1L);
    quiz.setName("Quiz 1");
    quiz.setDescription("Description 1");
    quiz.setDifficulty(1);
    quiz.setCreator(userDB);
    quiz.setTags(tags);

    Quiz quiz2 = new Quiz();
    quiz2.setId(2L);
    quiz2.setName("Quiz 2");
    quiz2.setDescription("Description 2");
    quiz2.setDifficulty(2);
    quiz2.setCreator(userDB);
    quiz2.setTags(tags);

    quizRepository.saveAll(List.of(quiz, quiz2));

    long quizId = 1L;

    String response = mvc.perform(get("/api/tag/quiz")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Long.toString(quizId)))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();

    List<TagDTO> returnedDTOs = mapper.readerForListOf(TagDTO.class).readValue(response);

    for (int i = 0; i < returnedDTOs.size(); i++) {
      assertEquals(tags.get(i).getId(), returnedDTOs.get(i).getId());
      assertEquals(tags.get(i).getTag(), returnedDTOs.get(i).getTag());
    }

    quizId = 2L;

    response = mvc.perform(get("/api/tag/quiz")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Long.toString(quizId)))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();

    returnedDTOs = mapper.readerForListOf(TagDTO.class).readValue(response);

    for (int i = 0; i < returnedDTOs.size(); i++) {
      assertEquals(tags.get(i).getId(), returnedDTOs.get(i).getId());
      assertEquals(tags.get(i).getTag(), returnedDTOs.get(i).getTag());
    }
  }

  @Test
  public void getQuestionTags_returnsAllTagsForQuestion() throws Exception {

    List<Tag> tags = Arrays.asList(
            new Tag("Tag 1"),
            new Tag("Tag 2"),
            new Tag("Tag 3")
    );

    List<Tag> tagsDB = dbTag.saveAll(tags);

    User user = new User();
    user.setId(1L);
    user.setUsername("FireLights99");
    user.setPassword("password");
    user.setEmail("fire@gmail.com");

    User userDB = userRepository.save(user);

    Question question = new Question();

    question.setId(1L);
    question.setText("Question text 1");
    question.setTags(tagsDB);
    question.setAnswers(new ArrayList<>());
    question.setQuizzes(new ArrayList<>());
    question.setResource_url("Test resource URL 1");
    question.setCreator(userDB);

    questionRepository.save(question);

    long questionId = 1L;

    String response = mvc.perform(get("/api/tag/question")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Long.toString(questionId)))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();

    List<TagDTO> returnedDTOs = mapper.readerForListOf(TagDTO.class).readValue(response);

    for (int i = 0; i < returnedDTOs.size(); i++) {
      assertEquals(tags.get(i).getId(), returnedDTOs.get(i).getId());
      assertEquals(tags.get(i).getTag(), returnedDTOs.get(i).getTag());
    }
  }
}
