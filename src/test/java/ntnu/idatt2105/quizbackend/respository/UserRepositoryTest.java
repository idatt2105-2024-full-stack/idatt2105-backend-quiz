package ntnu.idatt2105.quizbackend.respository;

import jakarta.transaction.Transactional;
import ntnu.idatt2105.quizbackend.entity.User;
import ntnu.idatt2105.quizbackend.repository.UserRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@Transactional
public class UserRepositoryTest {

    @Autowired
    private UserRepository repository;

    private static final User mockUser1 = new User();

    @BeforeAll
    public static void setup() {
        mockUser1.setUsername("Hans-Tore");
        mockUser1.setEmail("hato@gmail.com");
        mockUser1.setPassword("Passopp123");
    }

    @Test
    public void findPasswordByEmail() throws Exception {
        repository.save(mockUser1);

        String password = repository.findPasswordByEmail(mockUser1.getEmail());

        assertEquals(mockUser1.getPassword(), password);
    }
}
