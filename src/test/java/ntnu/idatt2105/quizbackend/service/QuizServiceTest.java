package ntnu.idatt2105.quizbackend.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.transaction.Transactional;
import ntnu.idatt2105.quizbackend.dto.QuizDTO;
import ntnu.idatt2105.quizbackend.entity.*;
import ntnu.idatt2105.quizbackend.repository.*;
import ntnu.idatt2105.quizbackend.util.JwtUtil;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.TestPropertySource;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureDataJpa
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
@Import({QuizService.class})
@ExtendWith(MockitoExtension.class)
@Transactional
public class QuizServiceTest {

  @Autowired
  private QuizRepository quizRepository;

  @Autowired
  private QuestionRepository questionRepository;

  @Autowired
  private AnswerRepository answerRepository;

  @Autowired
  private TagRepository tagRepository;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private QuizService quizService;

  private static final ObjectMapper mapper = new ObjectMapper();
  private static final ModelMapper modelMapper = new ModelMapper();

  private static final User creator = new User();

  private static final Question question1 = new Question();
  private static final Question question2 = new Question();

  private static final Answer answer1 = new Answer();
  private static final Answer answer2 = new Answer();
  private static final Answer answer3 = new Answer();
  private static final Answer answer4 = new Answer();


  private static final Quiz quiz1 = new Quiz();
  private static final Quiz quiz2 = new Quiz();
  private static final Quiz quiz3 = new Quiz();
  private static final Quiz quiz4 = new Quiz();
  private static final Quiz quiz5 = new Quiz();
  private static final Quiz quiz6 = new Quiz();
  private static final Quiz quiz7 = new Quiz();

  private static final Tag tag1 = new Tag();
  private static final Tag tag2 = new Tag();
  private static final Tag tag3 = new Tag();
  private static final Tag tag4 = new Tag();


  @BeforeAll
  public static void setUp() {
    creator.setId(1L);
    creator.setUsername("testuser");
    creator.setEmail("test");
    creator.setPassword("password");

    question1.setId(1L);
    question1.setText("Test question 1");

    question2.setId(2L);
    question2.setText("Test question 2");

    answer1.setId(1L);
    answer1.setText("Test answer 1");
    answer1.setCorrectAnswer(true);

    answer2.setId(2L);
    answer2.setText("Test answer 2");
    answer2.setCorrectAnswer(false);

    answer3.setId(3L);
    answer3.setText("Test answer 1");
    answer3.setCorrectAnswer(true);

    answer4.setId(4L);
    answer4.setText("Test answer 2");
    answer4.setCorrectAnswer(false);

    question1.setCreator(creator);
    question2.setCreator(creator);

    quiz1.setId(1L);
    quiz1.setName("Quiz 1");
    quiz1.setDescription("Description of Quiz 1");
    quiz1.setCreator(creator);

    quiz2.setId(2L);
    quiz2.setName("Quiz 2");
    quiz2.setDescription("Description of Quiz 2");
    quiz2.setCreator(creator);

    quiz3.setId(3L);
    quiz3.setName("Quiz 3");
    quiz3.setDescription("Description of Quiz 3");
    quiz3.setCreator(creator);

    quiz4.setId(4L);
    quiz4.setName("Quiz 4");
    quiz4.setDescription("Description of Quiz 4");
    quiz4.setCreator(creator);

    quiz5.setId(5L);
    quiz5.setName("Quiz 5");
    quiz5.setDescription("Description of Quiz 5");
    quiz5.setCreator(creator);

    quiz6.setId(6L);
    quiz6.setName("Quiz 6");
    quiz6.setDescription("Description of Quiz 6");
    quiz6.setCreator(creator);

    quiz7.setId(7L);
    quiz7.setName("Quiz 7");
    quiz7.setDescription("Description of Quiz 7");
    quiz7.setCreator(creator);

    tag1.setId(1L);
    tag1.setTag("Tag 1");

    tag2.setId(2L);
    tag2.setTag("Tag 2");

    tag3.setId(3L);
    tag3.setTag("Tag 3");

    tag4.setId(4L);
    tag4.setTag("Tag 4");
  }

  @BeforeEach
  public void cleanDatabase() {
    tagRepository.deleteAll();
    answerRepository.deleteAll();
    quizRepository.deleteAll();
    questionRepository.deleteAll();

    userRepository.deleteAll();
  }

  @Test
  @Transactional
  public void getSixRandomQuizzesPaginated() {
    User userDB = userRepository.save(creator);

    question1.setCreator(userDB);
    question2.setCreator(userDB);

    List<Tag> tagsDB = tagRepository.saveAll(List.of(tag1, tag2, tag3, tag4));

    List<Question> questionDB = questionRepository.saveAll(List.of(question1, question2));

    quiz1.setQuestions(questionDB);
    quiz1.setCreator(userDB);
    quiz1.setTags(tagsDB);

    quiz2.setQuestions(questionDB);
    quiz2.setCreator(userDB);
    quiz2.setTags(tagsDB);

    quiz3.setQuestions(questionDB);
    quiz3.setCreator(userDB);
    quiz3.setTags(tagsDB);

    quiz4.setQuestions(questionDB);
    quiz4.setCreator(userDB);
    quiz4.setTags(tagsDB);

    quiz5.setQuestions(questionDB);
    quiz5.setCreator(userDB);
    quiz5.setTags(tagsDB);

    quiz6.setQuestions(questionDB);
    quiz6.setCreator(userDB);
    quiz6.setTags(tagsDB);

    quiz7.setQuestions(questionDB);
    quiz7.setCreator(userDB);
    quiz7.setTags(tagsDB);

    quizRepository.saveAll(List.of(quiz1, quiz2, quiz3, quiz4, quiz5, quiz6, quiz7));


    List<Quiz> quizzes = quizService.getSixRandomQuizzes();
    List<Quiz> quizzes2 = quizService.getSixRandomQuizzes();
    assertTrue(quizzes.size() <= 6);
    assertNotEquals(quizzes2, quizzes);
  }

  @Test
  @Transactional
  public void convertToQuizDTO() {
    User userDB = userRepository.save(creator);

    question1.setCreator(userDB);
    question2.setCreator(userDB);

    List<Tag> tagsDB = tagRepository.saveAll(List.of(tag1, tag2, tag3, tag4));

    List<Question> questionDB = questionRepository.saveAll(List.of(question1, question2));

    quiz1.setQuestions(questionDB);
    quiz1.setCreator(userDB);
    quiz1.setTags(tagsDB);

    QuizDTO quizDTO = quizService.convertToQuizDTO(quiz1);

    assertEquals(quizDTO.getId(), quiz1.getId());
    assertEquals(quizDTO.getName(), quiz1.getName());
    assertEquals(quizDTO.getDescription(), quiz1.getDescription());
  }

  @Test
  @Transactional
  public void getAllQuizzes() {
    User userDB = userRepository.save(creator);

    question1.setCreator(userDB);
    question2.setCreator(userDB);

    List<Tag> tagsDB = tagRepository.saveAll(List.of(tag1, tag2, tag3, tag4));

    List<Question> questionDB = questionRepository.saveAll(List.of(question1, question2));

    quiz1.setQuestions(questionDB);
    quiz1.setCreator(userDB);
    quiz1.setTags(tagsDB);

    quiz2.setQuestions(questionDB);
    quiz2.setCreator(userDB);
    quiz2.setTags(tagsDB);

    quiz3.setQuestions(questionDB);
    quiz3.setCreator(userDB);
    quiz3.setTags(tagsDB);

    quiz4.setQuestions(questionDB);
    quiz4.setCreator(userDB);
    quiz4.setTags(tagsDB);

    quiz5.setQuestions(questionDB);
    quiz5.setCreator(userDB);
    quiz5.setTags(tagsDB);

    quiz6.setQuestions(questionDB);
    quiz6.setCreator(userDB);
    quiz6.setTags(tagsDB);

    quiz7.setQuestions(questionDB);
    quiz7.setCreator(userDB);
    quiz7.setTags(tagsDB);

    quizRepository.saveAll(List.of(quiz1, quiz2, quiz3, quiz4, quiz5, quiz6, quiz7));

    List<Quiz> quizzes = quizService.getQuizzes();
    assertEquals(7, quizzes.size());
  }

  @Test
  @Transactional
  public void getQuizById_ReturnQuiz() {
    User userDB = userRepository.save(creator);

    question1.setCreator(userDB);
    question2.setCreator(userDB);

    // List<Tag> tagsDB = tagRepository.saveAll(List.of(tag1, tag2, tag3, tag4));

    List<Tag> tagsDB = new ArrayList<>();

    List<Question> questionDB = questionRepository.saveAll(List.of(question1, question2));

    quiz1.setQuestions(questionDB);
    quiz1.setCreator(userDB);
    quiz1.setTags(tagsDB);

    quiz2.setQuestions(questionDB);
    quiz2.setCreator(userDB);
    quiz2.setTags(tagsDB);

    quiz3.setQuestions(questionDB);
    quiz3.setCreator(userDB);
    quiz3.setTags(tagsDB);

    quiz4.setQuestions(questionDB);
    quiz4.setCreator(userDB);
    quiz4.setTags(tagsDB);

    quiz5.setQuestions(questionDB);
    quiz5.setCreator(userDB);
    quiz5.setTags(tagsDB);

    quiz6.setQuestions(questionDB);
    quiz6.setCreator(userDB);
    quiz6.setTags(tagsDB);

    quiz7.setQuestions(questionDB);
    quiz7.setCreator(userDB);
    quiz7.setTags(tagsDB);

    List<Quiz> quizzesDB = quizRepository.saveAll(List.of(quiz1, quiz2, quiz3, quiz4, quiz5, quiz6, quiz7));

    Quiz quiz = quizService.getQuizById(quizzesDB.get(0).getId());
    System.out.println(quiz.getId());
    assertEquals(quizzesDB.get(0).getId(), quiz.getId());
  }

  @Test
  @Transactional
  public void getQuizById_ReturnNull() {
    User userDB = userRepository.save(creator);

    question1.setCreator(userDB);
    question2.setCreator(userDB);

    List<Tag> tagsDB = tagRepository.saveAll(List.of(tag1, tag2, tag3, tag4));

    List<Question> questionDB = questionRepository.saveAll(List.of(question1, question2));

    quiz1.setQuestions(questionDB);
    quiz1.setCreator(userDB);
    quiz1.setTags(tagsDB);

    quiz2.setQuestions(questionDB);
    quiz2.setCreator(userDB);
    quiz2.setTags(tagsDB);

    quiz3.setQuestions(questionDB);
    quiz3.setCreator(userDB);
    quiz3.setTags(tagsDB);

    quiz4.setQuestions(questionDB);
    quiz4.setCreator(userDB);
    quiz4.setTags(tagsDB);

    quiz5.setQuestions(questionDB);
    quiz5.setCreator(userDB);
    quiz5.setTags(tagsDB);

    quiz6.setQuestions(questionDB);
    quiz6.setCreator(userDB);
    quiz6.setTags(tagsDB);

    quiz7.setQuestions(questionDB);
    quiz7.setCreator(userDB);
    quiz7.setTags(tagsDB);

    quizRepository.saveAll(List.of(quiz1, quiz2, quiz3, quiz4, quiz5, quiz6, quiz7));

    Quiz quiz = quizService.getQuizById(100L);
    assertNull(quiz);
  }

  @Test
  @Transactional
  public void getQuizzesByUser() {
    User userDB = userRepository.save(creator);

    question1.setCreator(userDB);
    question2.setCreator(userDB);

    List<Tag> tagsDB = tagRepository.saveAll(List.of(tag1, tag2, tag3, tag4));

    List<Question> questionDB = questionRepository.saveAll(List.of(question1, question2));

    quiz1.setQuestions(questionDB);
    quiz1.setCreator(userDB);
    quiz1.setTags(tagsDB);

    quiz2.setQuestions(questionDB);
    quiz2.setCreator(userDB);
    quiz2.setTags(tagsDB);

    quiz3.setQuestions(questionDB);
    quiz3.setCreator(userDB);
    quiz3.setTags(tagsDB);

    quiz4.setQuestions(questionDB);
    quiz4.setCreator(userDB);
    quiz4.setTags(tagsDB);

    quiz5.setQuestions(questionDB);
    quiz5.setCreator(userDB);
    quiz5.setTags(tagsDB);

    quiz6.setQuestions(questionDB);
    quiz6.setCreator(userDB);
    quiz6.setTags(tagsDB);

    quiz7.setQuestions(questionDB);
    quiz7.setCreator(userDB);
    quiz7.setTags(tagsDB);

    quizRepository.saveAll(List.of(quiz1, quiz2, quiz3, quiz4, quiz5, quiz6, quiz7));

    SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(
            userDB.getId(),
            null,
            Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER"))));

    List<Quiz> quizzes = quizService.getAllQuizzesByUser();
    assertEquals(7, quizzes.size());
  }
}
