package ntnu.idatt2105.quizbackend.service;

import ntnu.idatt2105.quizbackend.entity.Answer;
import ntnu.idatt2105.quizbackend.entity.Attempt;
import ntnu.idatt2105.quizbackend.entity.Question;
import ntnu.idatt2105.quizbackend.entity.Quiz;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class QuizGameTest {

  private static final Quiz mockQuiz = new Quiz();
  private static final Question mockQuestion1 = new Question();
  private static final Question mockQuestion2 = new Question();
  private static final Question mockQuestion3 = new Question();

  @BeforeAll
  public static void setupQuizEntity() {
    mockQuestion1.setAnswers(List.of(new Answer[]{
            new Answer("Correct answer", true),
            new Answer("Wrong answer", false),
            new Answer("Wrong answer", false)
    }));

    mockQuestion2.setAnswers(List.of(new Answer[]{
            new Answer("Wrong answer", false),
            new Answer("Correct answer", true),
            new Answer("Wrong answer", false)
    }));

    mockQuestion3.setAnswers(List.of(new Answer[]{
            new Answer("Wrong answer", false),
            new Answer("Wrong answer", false),
            new Answer("Correct answer", true)
    }));

    mockQuestion1.setText("Question 1");
    mockQuestion2.setText("Question 2");
    mockQuestion3.setText("Question 3");

    mockQuiz.setQuestions(List.of(new Question[]{
            mockQuestion1,
            mockQuestion2,
            mockQuestion3
    }));
  }

  @Test
  public void submittingAnswer_returnsNullWhenNotFinished_andAttemptWhenFinished() {
    QuizGame q = new QuizGame(mockQuiz);

    Attempt return1 = q.submitAnswerAndGotoNextQuestion(0);
    Attempt return2 = q.submitAnswerAndGotoNextQuestion(1);
    Attempt return3 = q.submitAnswerAndGotoNextQuestion(2);

    assertNull(return1);
    assertNull(return2);
    assertNotNull(return3);
  }

  @Test
  public void quizMovesThroughAllQuestions() {
    QuizGame q = new QuizGame(mockQuiz);

    assertEquals(mockQuestion1, q.getCurrentQuestion());
    Attempt return1 = q.submitAnswerAndGotoNextQuestion(0);
    assertEquals(mockQuestion2, q.getCurrentQuestion());
    Attempt return2 = q.submitAnswerAndGotoNextQuestion(1);
    assertEquals(mockQuestion3, q.getCurrentQuestion());
    Attempt return3 = q.submitAnswerAndGotoNextQuestion(2);
    assertNull(q.getCurrentQuestion());
  }

  @Test
  public void quizReturnsCompletionStatusCorrectly() {
    QuizGame q = new QuizGame(mockQuiz);

    assertFalse(q.completed());
    Attempt return1 = q.submitAnswerAndGotoNextQuestion(0);
    assertFalse(q.completed());
    Attempt return2 = q.submitAnswerAndGotoNextQuestion(1);
    assertFalse(q.completed());
    Attempt return3 = q.submitAnswerAndGotoNextQuestion(2);
    assertTrue(q.completed());
  }

  @Test
  public void quizGameReturnsAttemptWhenFinished() {
    QuizGame q = new QuizGame(mockQuiz);

    q.submitAnswerAndGotoNextQuestion(0);
    q.submitAnswerAndGotoNextQuestion(1);
    q.submitAnswerAndGotoNextQuestion(2);

    assertNotNull(q.getAttemptEntity());
    assertEquals(q.getAttemptEntity(), q.submitAnswerAndGotoNextQuestion(2));
  }

  @Test
  public void quizGameStreakReset() {
    QuizGame q = new QuizGame(mockQuiz);

    q.submitAnswerAndGotoNextQuestion(0);
    q.submitAnswerAndGotoNextQuestion(1);
    q.submitAnswerAndGotoNextQuestion(0);

    assertEquals(1.0f, q.getStreakMultiplier());
  }

  @Test
  public void quizGameAttemptRetrieval_ReturnsNull() {
    QuizGame q = new QuizGame(mockQuiz);

    assertNull(q.getAttemptEntity());
  }

  @Test
  public void quizGameAttemptRetrieval_ReturnsNotNull() {
    QuizGame q = new QuizGame(mockQuiz);

    q.submitAnswerAndGotoNextQuestion(0);
    q.submitAnswerAndGotoNextQuestion(1);
    q.submitAnswerAndGotoNextQuestion(2);

    assertNotNull(q.getAttemptEntity());
  }

  @Test
  public void quizGameAttemptGetStartDate_ReturnsDate() {
    QuizGame q = new QuizGame(mockQuiz);
    Date startDate = new Date();

    q.submitAnswerAndGotoNextQuestion(0);
    q.submitAnswerAndGotoNextQuestion(1);
    q.submitAnswerAndGotoNextQuestion(2);

    assertNotNull(q.getStartDate());
    assertEquals(startDate, q.getStartDate());
  }
}
