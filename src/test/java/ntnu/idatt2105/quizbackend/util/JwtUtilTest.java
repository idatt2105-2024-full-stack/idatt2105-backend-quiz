package ntnu.idatt2105.quizbackend.util;


import com.auth0.jwt.interfaces.DecodedJWT;
import ntnu.idatt2105.quizbackend.security.SecretsConfig;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class JwtUtilTest {

    private final JwtUtil util = new JwtUtil(new SecretsConfig());

    @Test
    public void decodeCreatedToken_andReadUsername() {
        final String testName = "TestName";
        String token = util.createToken(testName);

        DecodedJWT decodedToken = util.decodeToken(token);
        assertEquals(decodedToken.getSubject(), testName);
    }

    @Test
    public void tokenDoesNotContainUsername() {
        final String testName = "TestName";
        String token = util.createToken(testName);

        assertFalse(token.contains(testName));
    }
}
