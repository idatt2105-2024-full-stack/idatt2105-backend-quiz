INSERT INTO users (id, email, password, token, username) VALUES (DEFAULT, "sensor@gmail.com", "12345678", "0", "sensor1"); 
INSERT INTO users (id, email, password, token, username) VALUES (DEFAULT, "spiderboy@gmail.com", "21345867", "0", "spiderboy45");
INSERT INTO users (id, email, password, token, username) VALUES (DEFAULT, "waynenoris06@gmail.com", "41235786", "0", "waynenoris06");

INSERT INTO quiz (quiz_id, description, difficulty, name, thumbnail, user_id) VALUES (DEFAULT, "Capitals of the given contries in the world", 5, "Capitals of the World", NULL, 1);  -- Id 1 
INSERT INTO quiz (quiz_id, description, difficulty, name, thumbnail, user_id) VALUES (DEFAULT, "Capitals of the given contries in the world", 5, "Capitals of the World", NULL, 2);  -- Id 2 
INSERT INTO quiz (quiz_id, description, difficulty, name, thumbnail, user_id) VALUES (DEFAULT, "Capitals of the given contries in the world", 5, "Capitals of the World", NULL, 1);  -- Id 3 
INSERT INTO quiz (quiz_id, description, difficulty, name, thumbnail, user_id) VALUES (DEFAULT, "Capitals of the given contries in the world", 5, "Capitals of the World", NULL, 2);  -- Id 4 
INSERT INTO quiz (quiz_id, description, difficulty, name, thumbnail, user_id) VALUES (DEFAULT, "Capitals of the given contries in the world", 5, "Capitals of the World", NULL, 1);  -- Id 5 
INSERT INTO quiz (quiz_id, description, difficulty, name, thumbnail, user_id) VALUES (DEFAULT, "Capitals of the given contries in the world", 5, "Capitals of the World", NULL, 2);  -- Id 6 
INSERT INTO quiz (quiz_id, description, difficulty, name, thumbnail, user_id) VALUES (DEFAULT, "Capitals of the given contries in the world", 5, "Capitals of the World", NULL, 1);  -- Id 7 
INSERT INTO quiz (quiz_id, description, difficulty, name, thumbnail, user_id) VALUES (DEFAULT, "Capitals of the given contries in the world", 5, "Capitals of the World", NULL, 2);  -- Id 8 
INSERT INTO quiz (quiz_id, description, difficulty, name, thumbnail, user_id) VALUES (DEFAULT, "Capitals of the given contries in the world", 5, "Capitals of the World", NULL, 1);  -- Id 9 
INSERT INTO quiz (quiz_id, description, difficulty, name, thumbnail, user_id) VALUES (DEFAULT, "Capitals of the given contries in the world", 5, "Capitals of the World", NULL, 2);  -- Id 10
INSERT INTO quiz (quiz_id, description, difficulty, name, thumbnail, user_id) VALUES (DEFAULT, "Capitals of the given contries in the world", 5, "Capitals of the World", NULL, 1);  -- Id 11
INSERT INTO quiz (quiz_id, description, difficulty, name, thumbnail, user_id) VALUES (DEFAULT, "Capitals of the given contries in the world", 5, "Capitals of the World", NULL, 2);  -- Id 12
INSERT INTO quiz (quiz_id, description, difficulty, name, thumbnail, user_id) VALUES (DEFAULT, "Capitals of the given contries in the world", 5, "Capitals of the World", NULL, 1);  -- Id 13
INSERT INTO quiz (quiz_id, description, difficulty, name, thumbnail, user_id) VALUES (DEFAULT, "Capitals of the given contries in the world", 5, "Capitals of the World", NULL, 2);  -- Id 14
INSERT INTO quiz (quiz_id, description, difficulty, name, thumbnail, user_id) VALUES (DEFAULT, "Capitals of the given contries in the world", 5, "Capitals of the World", NULL, 1);  -- Id 15
INSERT INTO quiz (quiz_id, description, difficulty, name, thumbnail, user_id) VALUES (DEFAULT, "Capitals of the given contries in the world", 5, "Capitals of the World", NULL, 2);  -- Id 16



-- Capitals of the World 1 questions and answers

-- Question 1
INSERT INTO question (question_id, resource_url, text, user_id) VALUES (DEFAULT, NULL, "What is the capital of Norway", 1);
INSERT INTO answer (answer_id, correct_answer, text, question_id) VALUES (DEFAULT, TRUE, "Oslo", 1); -- Id 1
INSERT INTO answer (answer_id, correct_answer, text, question_id) VALUES (DEFAULT, FALSE, "Kabul", 1); -- Id 2
INSERT INTO answer (answer_id, correct_answer, text, question_id) VALUES (DEFAULT, FALSE, "Stockholm", 1); -- Id 3
INSERT INTO answer (answer_id, correct_answer, text, question_id) VALUES (DEFAULT, FALSE, "Bern", 1); -- Id 4

-- Question 2
INSERT INTO question (question_id, resource_url, text, user_id) VALUES (DEFAULT, NULL, "What is the capital of the United States", 1);
INSERT INTO answer (answer_id, correct_answer, text, question_id) VALUES (DEFAULT, FALSE, "London", 2); -- Id 5
INSERT INTO answer (answer_id, correct_answer, text, question_id) VALUES (DEFAULT, TRUE, "Washington D.C.", 2); -- Id 6
INSERT INTO answer (answer_id, correct_answer, text, question_id) VALUES (DEFAULT, FALSE, "Madrid", 2); -- Id 7
INSERT INTO answer (answer_id, correct_answer, text, question_id) VALUES (DEFAULT, FALSE, "Seoul", 2); -- Id 8

-- Question 3
INSERT INTO question (question_id, resource_url, text, user_id) VALUES (DEFAULT, NULL, "What is the capital of Japan ", 2);
INSERT INTO answer (answer_id, correct_answer, text, question_id) VALUES (DEFAULT, FALSE, "Seoul", 3); -- Id 9
INSERT INTO answer (answer_id, correct_answer, text, question_id) VALUES (DEFAULT, FALSE, "London", 3); -- Id 10
INSERT INTO answer (answer_id, correct_answer, text, question_id) VALUES (DEFAULT, TRUE, "Tokyo", 3); -- Id 11
INSERT INTO answer (answer_id, correct_answer, text, question_id) VALUES (DEFAULT, FALSE, "Madrid", 3); -- Id 12

-- Question 4
INSERT INTO question (question_id, resource_url, text, user_id) VALUES (DEFAULT, NULL, "What is the capital of Great Britain", 2);
INSERT INTO answer (answer_id, correct_answer, text, question_id) VALUES (DEFAULT, FALSE, "Tokyo", 4); -- Id 13
INSERT INTO answer (answer_id, correct_answer, text, question_id) VALUES (DEFAULT, FALSE, "Madrid", 4); -- Id 14
INSERT INTO answer (answer_id, correct_answer, text, question_id) VALUES (DEFAULT, FALSE, "Seoul", 4); -- Id 15
INSERT INTO answer (answer_id, correct_answer, text, question_id) VALUES (DEFAULT, TRUE, "London", 4); -- Id 16

-- Question 5
INSERT INTO question (question_id, resource_url, text, user_id) VALUES (DEFAULT, NULL, "What is the capital of Swenden", 1);
INSERT INTO answer (answer_id, correct_answer, text, question_id) VALUES (DEFAULT, FALSE, "Oslo", 5); -- Id 17
INSERT INTO answer (answer_id, correct_answer, text, question_id) VALUES (DEFAULT, FALSE, "Kabul", 5); -- Id 18
INSERT INTO answer (answer_id, correct_answer, text, question_id) VALUES (DEFAULT, TRUE, "Stockholm", 5); -- Id 19
INSERT INTO answer (answer_id, correct_answer, text, question_id) VALUES (DEFAULT, FALSE, "Bern", 5); -- Id 20

-- Connecting Quiz and questions
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (1, 1);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (1, 2);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (1, 3);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (1, 4);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (1, 5);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (2, 1);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (2, 2);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (2, 3);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (2, 4);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (2, 5);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (3, 1);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (3, 2);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (3, 3);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (3, 4);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (3, 5);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (4, 1);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (4, 2);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (4, 3);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (4, 4);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (4, 5);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (5, 1);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (5, 2);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (5, 3);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (5, 4);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (5, 5);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (6, 1);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (6, 2);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (6, 3);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (6, 4);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (6, 5);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (7, 1);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (7, 2);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (7, 3);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (7, 4);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (7, 5);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (8, 1);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (8, 2);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (8, 3);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (8, 4);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (8, 5);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (9, 1);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (9, 2);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (9, 3);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (9, 4);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (9, 5);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (2, 1);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (10, 2);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (10, 3);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (10, 4);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (10, 5);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (11, 1);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (11, 2);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (11, 3);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (11, 4);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (11, 5);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (12, 1);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (12, 2);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (12, 3);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (12, 4);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (12, 5);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (12, 1);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (13, 2);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (13, 3);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (13, 4);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (13, 5);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (16, 1);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (16, 2);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (16, 3);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (16, 4);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (16, 5);


-- Tags
INSERT INTO tag (tag_id, tag) VALUES (DEFAULT, "Trivia");
INSERT INTO tag (tag_id, tag) VALUES (DEFAULT, "Math");

-- Connecting Quizzes and Tag 1
INSERT INTO quiz_tags (quiz_id, tag_id) VALUES (1, 1);
INSERT INTO quiz_tags (quiz_id, tag_id) VALUES (2, 1);
INSERT INTO quiz_tags (quiz_id, tag_id) VALUES (3, 1); 
INSERT INTO quiz_tags (quiz_id, tag_id) VALUES (4, 1); 
INSERT INTO quiz_tags (quiz_id, tag_id) VALUES (5, 1);
INSERT INTO quiz_tags (quiz_id, tag_id) VALUES (6, 1); 
INSERT INTO quiz_tags (quiz_id, tag_id) VALUES (7, 1); 
INSERT INTO quiz_tags (quiz_id, tag_id) VALUES (8, 1); 
INSERT INTO quiz_tags (quiz_id, tag_id) VALUES (9, 1);
INSERT INTO quiz_tags (quiz_id, tag_id) VALUES (10, 1);
INSERT INTO quiz_tags (quiz_id, tag_id) VALUES (11, 1);
INSERT INTO quiz_tags (quiz_id, tag_id) VALUES (12, 1);
INSERT INTO quiz_tags (quiz_id, tag_id) VALUES (13, 1);
INSERT INTO quiz_tags (quiz_id, tag_id) VALUES (14, 1);
INSERT INTO quiz_tags (quiz_id, tag_id) VALUES (15, 1);
INSERT INTO quiz_tags (quiz_id, tag_id) VALUES (16, 1);

-- Connecting Questions and Tag 1
INSERT INTO question_tags (question_id, tag_id) VALUES (1, 1);
INSERT INTO question_tags (question_id, tag_id) VALUES (2, 1);
INSERT INTO question_tags (question_id, tag_id) VALUES (3, 1);
INSERT INTO question_tags (question_id, tag_id) VALUES (4, 1);
INSERT INTO question_tags (question_id, tag_id) VALUES (5, 1);


-- Attempt 1 for User 1 on Quiz 1
INSERT INTO attempt (attempt_id, date, quiz_name, score, attempter, quiz_id) VALUES (DEFAULT, '2023-12-31', "Capitals of the World", 320, 1, 1);
INSERT INTO attempt (attempt_id, date, quiz_name, score, attempter, quiz_id) VALUES (DEFAULT, '2023-12-30', "Capitals of the World", 620, 1, 2);


-- Connecting Attempt 1 from User 1 to Answers
INSERT INTO attempt_answers (attempt_id, answer_id) VALUES (1, 1);
INSERT INTO attempt_answers (attempt_id, answer_id) VALUES (1, 5);
INSERT INTO attempt_answers (attempt_id, answer_id) VALUES (1, 9);
INSERT INTO attempt_answers (attempt_id, answer_id) VALUES (1, 13);
INSERT INTO attempt_answers (attempt_id, answer_id) VALUES (1, 17);

-- Connecting Attempt 2 from User 1 to Answers
INSERT INTO attempt_answers (attempt_id, answer_id) VALUES (2, 1);
INSERT INTO attempt_answers (attempt_id, answer_id) VALUES (2, 6);
INSERT INTO attempt_answers (attempt_id, answer_id) VALUES (2, 11);
INSERT INTO attempt_answers (attempt_id, answer_id) VALUES (2, 16);
INSERT INTO attempt_answers (attempt_id, answer_id) VALUES (2, 19);


-- Attempts for User 2 on Quiz 1
INSERT INTO attempt (attempt_id, date, quiz_name, score, attempter, quiz_id) VALUES (DEFAULT, '2023-12-31', "Capitals of the World", 320, 2, 1);
INSERT INTO attempt (attempt_id, date, quiz_name, score, attempter, quiz_id) VALUES (DEFAULT, '2023-12-30', "Capitals of the World", 620, 2, 2);


-- Connecting Attempt 1 from User 1 to Answers
INSERT INTO attempt_answers (attempt_id, answer_id) VALUES (3, 1);
INSERT INTO attempt_answers (attempt_id, answer_id) VALUES (3, 9);
INSERT INTO attempt_answers (attempt_id, answer_id) VALUES (3, 5);
INSERT INTO attempt_answers (attempt_id, answer_id) VALUES (3, 13);
INSERT INTO attempt_answers (attempt_id, answer_id) VALUES (3, 17);

-- Connecting Attempt 2 from User 1 to Answers
INSERT INTO attempt_answers (attempt_id, answer_id) VALUES (4, 1);
INSERT INTO attempt_answers (attempt_id, answer_id) VALUES (4, 6);
INSERT INTO attempt_answers (attempt_id, answer_id) VALUES (4, 11);
INSERT INTO attempt_answers (attempt_id, answer_id) VALUES (4, 16);
INSERT INTO attempt_answers (attempt_id, answer_id) VALUES (4, 19);



-- Math Quizzes
INSERT INTO quiz (quiz_id, description, difficulty, name, thumbnail, user_id) VALUES (DEFAULT, "Can you solve these math problems", 2, "5th Grade Math", NULL, 2); -- Id 17
INSERT INTO quiz (quiz_id, description, difficulty, name, thumbnail, user_id) VALUES (DEFAULT, "Can you solve these math problems", 2, "5th Grade Math", NULL, 3); -- Id 18

-- Math Questions
-- Question 6
INSERT INTO question (question_id, resource_url, text, user_id) VALUES (DEFAULT, NULL, "2 + 2", 2);
INSERT INTO answer (answer_id, correct_answer, text, question_id) VALUES (DEFAULT, TRUE, "4", 6);
INSERT INTO answer (answer_id, correct_answer, text, question_id) VALUES (DEFAULT, FALSE, "6", 6);

-- Question 7
INSERT INTO question (question_id, resource_url, text, user_id) VALUES (DEFAULT, NULL, "4 + 2", 2);
INSERT INTO answer (answer_id, correct_answer, text, question_id) VALUES (DEFAULT, FALSE, "4", 7);
INSERT INTO answer (answer_id, correct_answer, text, question_id) VALUES (DEFAULT, TRUE, "6", 7);


-- Connecting Math Quiz 17 with Math Questions 6 and 7
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (17, 6);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (17, 7);

-- Connecting Math Quiz 18 with Math Questions 6 and 7
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (18, 6);
INSERT INTO quiz_questions (quiz_id, question_id) VALUES (18, 7);


-- Connecting Math Quiz 17 & 18 with Tag 2
INSERT INTO quiz_tags (quiz_id, tag_id) VALUES (17, 2);
INSERT INTO quiz_tags (quiz_id, tag_id) VALUES (18, 2);

-- Connecting Math Questions 6 & 7 with Tag 2
INSERT INTO quiz_tags (quiz_id, tag_id) VALUES (6, 2);
INSERT INTO quiz_tags (quiz_id, tag_id) VALUES (7, 2);